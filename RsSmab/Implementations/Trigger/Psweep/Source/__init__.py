from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup
from .....Internal import Conversions
from ..... import enums
from ..... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class SourceCls:
	"""Source commands group definition. 2 total commands, 1 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("source", core, parent)

	@property
	def advanced(self):
		"""advanced commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_advanced'):
			from .Advanced import AdvancedCls
			self._advanced = AdvancedCls(self._core, self._cmd_group)
		return self._advanced

	def set(self, source: enums.SingExtAuto, inputIx=repcap.InputIx.Default) -> None:
		"""SCPI: TRIGger<HW>:PSWeep:SOURce \n
		Snippet: driver.trigger.psweep.source.set(source = enums.SingExtAuto.AUTO, inputIx = repcap.InputIx.Default) \n
			INTRO_CMD_HELP: Selects the trigger source for the corresponding sweeps: \n
			- FSWeep - RF frequency
			- LFFSweep - LF frequency
			- PSWeep - RF level
			- SWEep - all sweeps
		The source names of the parameters correspond to the values provided in manual control of the instrument. They differ
		from the SCPI-compliant names, but the instrument accepts both variants. Use the SCPI name, if compatibility is an
		important issue. Find the corresponding SCPI-compliant commands in Cross-reference between the manual and remote control. \n
			:param source: AUTO| IMMediate | SINGle| BUS | EXTernal | EAUTo AUTO [IMMediate] Executes a sweep automatically. In this free-running mode, the trigger condition is met continuously. I.e. when a sweep is completed, the next one starts immediately. SINGle [BUS] Executes one complete sweep cycle. The following commands initiate a trigger event: *TRG [:SOURcehw]:SWEep:POWer:EXECute [:SOURcehw]:SWEep[:FREQuency]:EXECute method RsSmab.Trigger.Sweep.Immediate.set, method RsSmab.Trigger.Psweep.Immediate.set and method RsSmab.Trigger.FreqSweep.Immediate.set. Set the sweep mode with the commands: [:SOURcehw]:SWEep:POWer:MODEAUTO|STEP [:SOURcehw]:SWEep[:FREQuency]:MODEAUTO|STEP [:SOURcehw]:LFOutput:SWEep[:FREQuency]:MODEAUTO|STEP In step mode (STEP) , the instrument executes only one step. EXTernal An external signal triggers the sweep. EAUTo An external signal triggers the sweep. When one sweep is finished, the next sweep starts. A second trigger event stops the sweep at the current frequency, a third trigger event starts the trigger at the start frequency, and so on.
			:param inputIx: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Trigger')
		"""
		param = Conversions.enum_scalar_to_str(source, enums.SingExtAuto)
		inputIx_cmd_val = self._cmd_group.get_repcap_cmd_value(inputIx, repcap.InputIx)
		self._core.io.write(f'TRIGger{inputIx_cmd_val}:PSWeep:SOURce {param}')

	# noinspection PyTypeChecker
	def get(self, inputIx=repcap.InputIx.Default) -> enums.SingExtAuto:
		"""SCPI: TRIGger<HW>:PSWeep:SOURce \n
		Snippet: value: enums.SingExtAuto = driver.trigger.psweep.source.get(inputIx = repcap.InputIx.Default) \n
			INTRO_CMD_HELP: Selects the trigger source for the corresponding sweeps: \n
			- FSWeep - RF frequency
			- LFFSweep - LF frequency
			- PSWeep - RF level
			- SWEep - all sweeps
		The source names of the parameters correspond to the values provided in manual control of the instrument. They differ
		from the SCPI-compliant names, but the instrument accepts both variants. Use the SCPI name, if compatibility is an
		important issue. Find the corresponding SCPI-compliant commands in Cross-reference between the manual and remote control. \n
			:param inputIx: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Trigger')
			:return: source: AUTO| IMMediate | SINGle| BUS | EXTernal | EAUTo AUTO [IMMediate] Executes a sweep automatically. In this free-running mode, the trigger condition is met continuously. I.e. when a sweep is completed, the next one starts immediately. SINGle [BUS] Executes one complete sweep cycle. The following commands initiate a trigger event: *TRG [:SOURcehw]:SWEep:POWer:EXECute [:SOURcehw]:SWEep[:FREQuency]:EXECute method RsSmab.Trigger.Sweep.Immediate.set, method RsSmab.Trigger.Psweep.Immediate.set and method RsSmab.Trigger.FreqSweep.Immediate.set. Set the sweep mode with the commands: [:SOURcehw]:SWEep:POWer:MODEAUTO|STEP [:SOURcehw]:SWEep[:FREQuency]:MODEAUTO|STEP [:SOURcehw]:LFOutput:SWEep[:FREQuency]:MODEAUTO|STEP In step mode (STEP) , the instrument executes only one step. EXTernal An external signal triggers the sweep. EAUTo An external signal triggers the sweep. When one sweep is finished, the next sweep starts. A second trigger event stops the sweep at the current frequency, a third trigger event starts the trigger at the start frequency, and so on."""
		inputIx_cmd_val = self._cmd_group.get_repcap_cmd_value(inputIx, repcap.InputIx)
		response = self._core.io.query_str(f'TRIGger{inputIx_cmd_val}:PSWeep:SOURce?')
		return Conversions.str_to_scalar_enum(response, enums.SingExtAuto)

	def clone(self) -> 'SourceCls':
		"""Clones the group by creating new object from it and its whole existing subgroups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = SourceCls(self._core, self._cmd_group.parent)
		self._cmd_group.synchronize_repcaps(new_group)
		return new_group
