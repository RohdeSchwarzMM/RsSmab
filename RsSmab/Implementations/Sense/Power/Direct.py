from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup
from ....Internal import Conversions
from .... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class DirectCls:
	"""Direct commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("direct", core, parent)

	def set(self, command: str, channel=repcap.Channel.Default) -> None:
		"""SCPI: SENSe<CH>:[POWer]:DIRect \n
		Snippet: driver.sense.power.direct.set(command = 'abc', channel = repcap.Channel.Default) \n
		No command help available \n
			:param command: No help available
			:param channel: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Sense')
		"""
		param = Conversions.value_to_quoted_str(command)
		channel_cmd_val = self._cmd_group.get_repcap_cmd_value(channel, repcap.Channel)
		self._core.io.write(f'SENSe{channel_cmd_val}:POWer:DIRect {param}')
