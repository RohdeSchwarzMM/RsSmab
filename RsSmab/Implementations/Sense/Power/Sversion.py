from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup
from ....Internal.Utilities import trim_str_response
from .... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class SversionCls:
	"""Sversion commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("sversion", core, parent)

	def get(self, channel=repcap.Channel.Default) -> str:
		"""SCPI: SENSe<CH>:[POWer]:SVERsion \n
		Snippet: value: str = driver.sense.power.sversion.get(channel = repcap.Channel.Default) \n
		Queries the software version of the connected R&S NRP power sensor. \n
			:param channel: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Sense')
			:return: sversion: string"""
		channel_cmd_val = self._cmd_group.get_repcap_cmd_value(channel, repcap.Channel)
		response = self._core.io.query_str(f'SENSe{channel_cmd_val}:POWer:SVERsion?')
		return trim_str_response(response)
