from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup
from .....Internal import Conversions
from ..... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class VoltageCls:
	"""Voltage commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("voltage", core, parent)

	def set(self, voltage: float, lfOutput=repcap.LfOutput.Default) -> None:
		"""SCPI: [SOURce]:LFOutput<CH>:INTernal:VOLTage \n
		Snippet: driver.source.lfOutput.internal.voltage.set(voltage = 1.0, lfOutput = repcap.LfOutput.Default) \n
		Sets the output voltage for the LF generators. The sum of both values must not exceed the overall output voltage, set
		with command [:SOURce]:LFOutput:VOLTage. \n
			:param voltage: float Range: 0 to 4
			:param lfOutput: optional repeated capability selector. Default value: Nr1 (settable in the interface 'LfOutput')
		"""
		param = Conversions.decimal_value_to_str(voltage)
		lfOutput_cmd_val = self._cmd_group.get_repcap_cmd_value(lfOutput, repcap.LfOutput)
		self._core.io.write(f'SOURce:LFOutput{lfOutput_cmd_val}:INTernal:VOLTage {param}')

	def get(self, lfOutput=repcap.LfOutput.Default) -> float:
		"""SCPI: [SOURce]:LFOutput<CH>:INTernal:VOLTage \n
		Snippet: value: float = driver.source.lfOutput.internal.voltage.get(lfOutput = repcap.LfOutput.Default) \n
		Sets the output voltage for the LF generators. The sum of both values must not exceed the overall output voltage, set
		with command [:SOURce]:LFOutput:VOLTage. \n
			:param lfOutput: optional repeated capability selector. Default value: Nr1 (settable in the interface 'LfOutput')
			:return: voltage: float Range: 0 to 4"""
		lfOutput_cmd_val = self._cmd_group.get_repcap_cmd_value(lfOutput, repcap.LfOutput)
		response = self._core.io.query_str(f'SOURce:LFOutput{lfOutput_cmd_val}:INTernal:VOLTage?')
		return Conversions.str_to_float(response)
