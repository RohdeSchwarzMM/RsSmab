from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup
from .....Internal import Conversions
from ..... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class ExternalCls:
	"""External commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("external", core, parent)

	# noinspection PyTypeChecker
	def get_impedance(self) -> enums.InpImpRf:
		"""SCPI: [SOURce<HW>]:PULM:TRIGger:EXTernal:IMPedance \n
		Snippet: value: enums.InpImpRf = driver.source.pulm.trigger.external.get_impedance() \n
		No command help available \n
			:return: impedance: No help available
		"""
		response = self._core.io.query_str('SOURce<HwInstance>:PULM:TRIGger:EXTernal:IMPedance?')
		return Conversions.str_to_scalar_enum(response, enums.InpImpRf)

	def set_impedance(self, impedance: enums.InpImpRf) -> None:
		"""SCPI: [SOURce<HW>]:PULM:TRIGger:EXTernal:IMPedance \n
		Snippet: driver.source.pulm.trigger.external.set_impedance(impedance = enums.InpImpRf.G10K) \n
		No command help available \n
			:param impedance: No help available
		"""
		param = Conversions.enum_scalar_to_str(impedance, enums.InpImpRf)
		self._core.io.write(f'SOURce<HwInstance>:PULM:TRIGger:EXTernal:IMPedance {param}')
