from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class FpFpgaCls:
	"""FpFpga commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("fpFpga", core, parent)

	def set_update(self, display_size: str) -> None:
		"""SCPI: SYSTem:FPFPga:UPDate \n
		Snippet: driver.system.fpFpga.set_update(display_size = 'abc') \n
		No command help available \n
			:param display_size: No help available
		"""
		param = Conversions.value_to_quoted_str(display_size)
		self._core.io.write(f'SYSTem:FPFPga:UPDate {param}')
