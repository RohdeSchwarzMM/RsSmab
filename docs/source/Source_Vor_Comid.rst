Comid
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:VOR:COMid:DASH
	single: [SOURce<HW>]:VOR:COMid:DEPTh
	single: [SOURce<HW>]:VOR:COMid:DOT
	single: [SOURce<HW>]:VOR:COMid:FREQuency
	single: [SOURce<HW>]:VOR:COMid:LETTer
	single: [SOURce<HW>]:VOR:COMid:PERiod
	single: [SOURce<HW>]:VOR:COMid:REPeat
	single: [SOURce<HW>]:VOR:COMid:SYMBol
	single: [SOURce<HW>]:VOR:COMid:TSCHema
	single: [SOURce<HW>]:VOR:COMid:[STATe]

.. code-block:: python

	[SOURce<HW>]:VOR:COMid:DASH
	[SOURce<HW>]:VOR:COMid:DEPTh
	[SOURce<HW>]:VOR:COMid:DOT
	[SOURce<HW>]:VOR:COMid:FREQuency
	[SOURce<HW>]:VOR:COMid:LETTer
	[SOURce<HW>]:VOR:COMid:PERiod
	[SOURce<HW>]:VOR:COMid:REPeat
	[SOURce<HW>]:VOR:COMid:SYMBol
	[SOURce<HW>]:VOR:COMid:TSCHema
	[SOURce<HW>]:VOR:COMid:[STATe]



.. autoclass:: RsSmab.Implementations.Source.Vor.Comid.ComidCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.vor.comid.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Vor_Comid_Code.rst