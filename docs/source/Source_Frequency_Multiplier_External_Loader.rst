Loader
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:LOADer:VERSion

.. code-block:: python

	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:LOADer:VERSion



.. autoclass:: RsSmab.Implementations.Source.Frequency.Multiplier.External.Loader.LoaderCls
	:members:
	:undoc-members:
	:noindex: