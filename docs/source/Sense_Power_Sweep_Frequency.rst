Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:[POWer]:SWEep:FREQuency:RMODe
	single: SENSe:[POWer]:SWEep:FREQuency:STARt
	single: SENSe:[POWer]:SWEep:FREQuency:STEPs
	single: SENSe:[POWer]:SWEep:FREQuency:STOP

.. code-block:: python

	SENSe:[POWer]:SWEep:FREQuency:RMODe
	SENSe:[POWer]:SWEep:FREQuency:STARt
	SENSe:[POWer]:SWEep:FREQuency:STEPs
	SENSe:[POWer]:SWEep:FREQuency:STOP



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_Frequency_Reference.rst
	Sense_Power_Sweep_Frequency_Sensor.rst
	Sense_Power_Sweep_Frequency_Spacing.rst
	Sense_Power_Sweep_Frequency_Timing.rst
	Sense_Power_Sweep_Frequency_Yscale.rst