Stage
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALibration:LEVel:OPU:STAGe:MODE
	single: CALibration:LEVel:OPU:STAGe:SUB
	single: CALibration:LEVel:OPU:STAGe

.. code-block:: python

	CALibration:LEVel:OPU:STAGe:MODE
	CALibration:LEVel:OPU:STAGe:SUB
	CALibration:LEVel:OPU:STAGe



.. autoclass:: RsSmab.Implementations.Calibration.Level.Opu.Stage.StageCls
	:members:
	:undoc-members:
	:noindex: