Pow
----------------------------------------





.. autoclass:: RsSmab.Implementations.Trace.Pow.PowCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trace.pow.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trace_Pow_Sweep.rst