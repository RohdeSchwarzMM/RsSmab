Immediate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:PULM:[INTernal]:[TRAin]:TRIGger:IMMediate

.. code-block:: python

	[SOURce]:PULM:[INTernal]:[TRAin]:TRIGger:IMMediate



.. autoclass:: RsSmab.Implementations.Source.Pulm.Internal.Train.Trigger.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex: