User
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: OUTPut:USER:MARKer

.. code-block:: python

	OUTPut:USER:MARKer



.. autoclass:: RsSmab.Implementations.Output.User.UserCls
	:members:
	:undoc-members:
	:noindex: