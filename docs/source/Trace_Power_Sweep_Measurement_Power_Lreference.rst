Lreference
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:MEASurement:POWer:LREFerence

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:MEASurement:POWer:LREFerence



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Power.Lreference.LreferenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trace.power.sweep.measurement.power.lreference.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trace_Power_Sweep_Measurement_Power_Lreference_Display.rst