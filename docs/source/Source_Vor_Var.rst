Var
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:VOR:VAR:FREQuency
	single: [SOURce<HW>]:VOR:VAR:[DEPTh]

.. code-block:: python

	[SOURce<HW>]:VOR:VAR:FREQuency
	[SOURce<HW>]:VOR:VAR:[DEPTh]



.. autoclass:: RsSmab.Implementations.Source.Vor.Var.VarCls
	:members:
	:undoc-members:
	:noindex: