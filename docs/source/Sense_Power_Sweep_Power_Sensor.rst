Sensor
----------------------------------------





.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Power.Sensor.SensorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.power.sensor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_Power_Sensor_Offset.rst
	Sense_Power_Sweep_Power_Sensor_Sfrequency.rst