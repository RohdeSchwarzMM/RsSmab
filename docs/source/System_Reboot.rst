Reboot
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:REBoot

.. code-block:: python

	SYSTem:REBoot



.. autoclass:: RsSmab.Implementations.System.Reboot.RebootCls
	:members:
	:undoc-members:
	:noindex: