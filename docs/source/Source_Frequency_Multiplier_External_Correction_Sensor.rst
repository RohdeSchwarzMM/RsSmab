Sensor<Channel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr64
	rc = driver.source.frequency.multiplier.external.correction.sensor.repcap_channel_get()
	driver.source.frequency.multiplier.external.correction.sensor.repcap_channel_set(repcap.Channel.Nr1)





.. autoclass:: RsSmab.Implementations.Source.Frequency.Multiplier.External.Correction.Sensor.SensorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.frequency.multiplier.external.correction.sensor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Frequency_Multiplier_External_Correction_Sensor_Sonce.rst