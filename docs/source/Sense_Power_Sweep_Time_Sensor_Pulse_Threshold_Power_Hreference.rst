Hreference
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:PULSe:THReshold:POWer:HREFerence

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:PULSe:THReshold:POWer:HREFerence



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.Sensor.Pulse.Threshold.Power.Hreference.HreferenceCls
	:members:
	:undoc-members:
	:noindex: