Negative
----------------------------------------





.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Transition.Negative.NegativeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trace.power.sweep.measurement.transition.negative.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trace_Power_Sweep_Measurement_Transition_Negative_Duration.rst
	Trace_Power_Sweep_Measurement_Transition_Negative_Occurrence.rst
	Trace_Power_Sweep_Measurement_Transition_Negative_Overshoot.rst