Yval
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:TIME:MATH<CH>:YVAL

.. code-block:: python

	CALCulate:[POWer]:SWEep:TIME:MATH<CH>:YVAL



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Time.Math.Yval.YvalCls
	:members:
	:undoc-members:
	:noindex: