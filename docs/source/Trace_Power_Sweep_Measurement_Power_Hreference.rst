Hreference
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:MEASurement:POWer:HREFerence

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:MEASurement:POWer:HREFerence



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Power.Hreference.HreferenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trace.power.sweep.measurement.power.hreference.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trace_Power_Sweep_Measurement_Power_Hreference_Display.rst