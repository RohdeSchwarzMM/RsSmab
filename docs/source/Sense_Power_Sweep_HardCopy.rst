HardCopy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:[POWer]:SWEep:HCOPy:DATA

.. code-block:: python

	SENSe:[POWer]:SWEep:HCOPy:DATA



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.HardCopy.HardCopyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.hardCopy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_HardCopy_Device.rst
	Sense_Power_Sweep_HardCopy_Execute.rst
	Sense_Power_Sweep_HardCopy_File.rst