Mode
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALibration:MODE:CONFiguration
	single: CALibration:MODE

.. code-block:: python

	CALibration:MODE:CONFiguration
	CALibration:MODE



.. autoclass:: RsSmab.Implementations.Calibration.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: