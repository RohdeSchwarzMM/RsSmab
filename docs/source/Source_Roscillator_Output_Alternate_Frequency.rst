Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:ROSCillator:OUTPut:ALTernate:FREQuency:MODE

.. code-block:: python

	[SOURce]:ROSCillator:OUTPut:ALTernate:FREQuency:MODE



.. autoclass:: RsSmab.Implementations.Source.Roscillator.Output.Alternate.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: