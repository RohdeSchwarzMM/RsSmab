SuPolicy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:SECurity:SUPolicy

.. code-block:: python

	SYSTem:SECurity:SUPolicy



.. autoclass:: RsSmab.Implementations.System.Security.SuPolicy.SuPolicyCls
	:members:
	:undoc-members:
	:noindex: