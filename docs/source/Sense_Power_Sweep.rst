Sweep
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:[POWer]:SWEep:ABORt
	single: SENSe:[POWer]:SWEep:INITiate
	single: SENSe:[POWer]:SWEep:MODE
	single: SENSe:[POWer]:SWEep:RMODe

.. code-block:: python

	SENSe:[POWer]:SWEep:ABORt
	SENSe:[POWer]:SWEep:INITiate
	SENSe:[POWer]:SWEep:MODE
	SENSe:[POWer]:SWEep:RMODe



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.SweepCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_Frequency.rst
	Sense_Power_Sweep_HardCopy.rst
	Sense_Power_Sweep_Power.rst
	Sense_Power_Sweep_Time.rst