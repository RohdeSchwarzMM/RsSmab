Marker
----------------------------------------





.. autoclass:: RsSmab.Implementations.Source.Sweep.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.sweep.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Sweep_Marker_Output.rst