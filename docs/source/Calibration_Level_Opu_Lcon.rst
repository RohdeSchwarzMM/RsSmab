Lcon
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALibration:LEVel:OPU:LCON:MODE

.. code-block:: python

	CALibration:LEVel:OPU:LCON:MODE



.. autoclass:: RsSmab.Implementations.Calibration.Level.Opu.Lcon.LconCls
	:members:
	:undoc-members:
	:noindex: