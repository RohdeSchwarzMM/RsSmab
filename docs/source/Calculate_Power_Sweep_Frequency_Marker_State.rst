State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:FREQuency:MARKer<CH>:STATe

.. code-block:: python

	CALCulate:[POWer]:SWEep:FREQuency:MARKer<CH>:STATe



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Frequency.Marker.State.StateCls
	:members:
	:undoc-members:
	:noindex: