Feed
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:TIME:MARKer<CH>:FEED

.. code-block:: python

	CALCulate:[POWer]:SWEep:TIME:MARKer<CH>:FEED



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Time.Marker.Feed.FeedCls
	:members:
	:undoc-members:
	:noindex: