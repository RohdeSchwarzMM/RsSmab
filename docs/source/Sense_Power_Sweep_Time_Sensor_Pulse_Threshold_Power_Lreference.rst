Lreference
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:PULSe:THReshold:POWer:LREFerence

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:PULSe:THReshold:POWer:LREFerence



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.Sensor.Pulse.Threshold.Power.Lreference.LreferenceCls
	:members:
	:undoc-members:
	:noindex: