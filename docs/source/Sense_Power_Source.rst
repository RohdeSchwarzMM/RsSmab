Source
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SOURce

.. code-block:: python

	SENSe<CH>:[POWer]:SOURce



.. autoclass:: RsSmab.Implementations.Sense.Power.Source.SourceCls
	:members:
	:undoc-members:
	:noindex: