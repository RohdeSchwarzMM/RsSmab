Update
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:FIRMware:UPDate

.. code-block:: python

	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:FIRMware:UPDate



.. autoclass:: RsSmab.Implementations.Source.Frequency.Multiplier.External.Firmware.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex: