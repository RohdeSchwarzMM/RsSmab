State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:SECurity:NETWork:SOE:[STATe]

.. code-block:: python

	SYSTem:SECurity:NETWork:SOE:[STATe]



.. autoclass:: RsSmab.Implementations.System.Security.Network.Soe.State.StateCls
	:members:
	:undoc-members:
	:noindex: