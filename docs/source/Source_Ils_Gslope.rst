Gslope
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:ILS:GSLope:PRESet
	single: [SOURce<HW>]:ILS:GSLope:STATe
	single: [SOURce<HW>]:ILS:[GSLope]:MODE
	single: [SOURce<HW>]:ILS:[GSLope]:PHASe
	single: [SOURce<HW>]:ILS:[GSLope]:SDM
	single: [SOURce<HW>]:ILS:[GSLope]:SOURce

.. code-block:: python

	[SOURce<HW>]:ILS:GSLope:PRESet
	[SOURce<HW>]:ILS:GSLope:STATe
	[SOURce<HW>]:ILS:[GSLope]:MODE
	[SOURce<HW>]:ILS:[GSLope]:PHASe
	[SOURce<HW>]:ILS:[GSLope]:SDM
	[SOURce<HW>]:ILS:[GSLope]:SOURce



.. autoclass:: RsSmab.Implementations.Source.Ils.Gslope.GslopeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.ils.gslope.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Ils_Gslope_Ddm.rst
	Source_Ils_Gslope_Frequency.rst
	Source_Ils_Gslope_Icao.rst
	Source_Ils_Gslope_Llobe.rst
	Source_Ils_Gslope_Ulobe.rst