Dlinearize
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALibration:LEVel:DLINearize:MODE

.. code-block:: python

	CALibration:LEVel:DLINearize:MODE



.. autoclass:: RsSmab.Implementations.Calibration.Level.Dlinearize.DlinearizeCls
	:members:
	:undoc-members:
	:noindex: