Power
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:[POWer]:SWEep:POWer:RMODe
	single: SENSe:[POWer]:SWEep:POWer:STARt
	single: SENSe:[POWer]:SWEep:POWer:STEPs
	single: SENSe:[POWer]:SWEep:POWer:STOP

.. code-block:: python

	SENSe:[POWer]:SWEep:POWer:RMODe
	SENSe:[POWer]:SWEep:POWer:STARt
	SENSe:[POWer]:SWEep:POWer:STEPs
	SENSe:[POWer]:SWEep:POWer:STOP



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_Power_Reference.rst
	Sense_Power_Sweep_Power_Sensor.rst
	Sense_Power_Sweep_Power_Spacing.rst
	Sense_Power_Sweep_Power_Timing.rst
	Sense_Power_Sweep_Power_Yscale.rst