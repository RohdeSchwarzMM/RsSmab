Continuous
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: INITiate<HW>:FSWeep:CONTinuous

.. code-block:: python

	INITiate<HW>:FSWeep:CONTinuous



.. autoclass:: RsSmab.Implementations.Initiate.FreqSweep.Continuous.ContinuousCls
	:members:
	:undoc-members:
	:noindex: