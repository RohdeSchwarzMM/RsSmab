Spacing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:[POWer]:SWEep:TIME:SPACing:[MODE]

.. code-block:: python

	SENSe:[POWer]:SWEep:TIME:SPACing:[MODE]



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.Spacing.SpacingCls
	:members:
	:undoc-members:
	:noindex: