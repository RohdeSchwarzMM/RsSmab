Power
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:CORRection:POWer:POINts
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:CORRection:POWer

.. code-block:: python

	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:CORRection:POWer:POINts
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:CORRection:POWer



.. autoclass:: RsSmab.Implementations.Source.Frequency.Multiplier.External.Correction.Power.PowerCls
	:members:
	:undoc-members:
	:noindex: