Freq
----------------------------------------





.. autoclass:: RsSmab.Implementations.Trace.Freq.FreqCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trace.freq.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trace_Freq_Sweep.rst