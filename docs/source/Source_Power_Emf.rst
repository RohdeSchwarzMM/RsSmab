Emf
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:POWer:EMF:STATe

.. code-block:: python

	[SOURce<HW>]:POWer:EMF:STATe



.. autoclass:: RsSmab.Implementations.Source.Power.Emf.EmfCls
	:members:
	:undoc-members:
	:noindex: