Lreference
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:PULSe:THReshold:POWer:LREFerence

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:PULSe:THReshold:POWer:LREFerence



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Pulse.Threshold.Power.Lreference.LreferenceCls
	:members:
	:undoc-members:
	:noindex: