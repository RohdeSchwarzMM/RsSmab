Sfrequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:POWer:[SENSor]:SFRequency

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:POWer:[SENSor]:SFRequency



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Power.Sensor.Sfrequency.SfrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.power.sensor.sfrequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_Power_Sensor_Sfrequency_State.rst