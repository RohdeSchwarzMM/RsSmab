Fproportional
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: OUTPut:FPRoportional:SCALe

.. code-block:: python

	OUTPut:FPRoportional:SCALe



.. autoclass:: RsSmab.Implementations.Output.Fproportional.FproportionalCls
	:members:
	:undoc-members:
	:noindex: