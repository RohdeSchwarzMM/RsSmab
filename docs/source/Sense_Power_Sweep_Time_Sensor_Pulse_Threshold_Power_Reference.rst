Reference
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:PULSe:THReshold:POWer:REFerence

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:PULSe:THReshold:POWer:REFerence



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.Sensor.Pulse.Threshold.Power.Reference.ReferenceCls
	:members:
	:undoc-members:
	:noindex: