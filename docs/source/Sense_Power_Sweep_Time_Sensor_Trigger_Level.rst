Level
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:TRIGger:LEVel

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:TRIGger:LEVel



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.Sensor.Trigger.Level.LevelCls
	:members:
	:undoc-members:
	:noindex: