Wrap
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:PROFiling:RECord:WRAP:STATe

.. code-block:: python

	SYSTem:PROFiling:RECord:WRAP:STATe



.. autoclass:: RsSmab.Implementations.System.Profiling.Record.Wrap.WrapCls
	:members:
	:undoc-members:
	:noindex: