Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:ILS:[GSLope]:FREQuency:MODE
	single: [SOURce<HW>]:ILS:[GSLope]:FREQuency:STEP
	single: [SOURce<HW>]:ILS:[GSLope]:FREQuency

.. code-block:: python

	[SOURce<HW>]:ILS:[GSLope]:FREQuency:MODE
	[SOURce<HW>]:ILS:[GSLope]:FREQuency:STEP
	[SOURce<HW>]:ILS:[GSLope]:FREQuency



.. autoclass:: RsSmab.Implementations.Source.Ils.Gslope.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: