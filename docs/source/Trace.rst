Trace<Trace>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.trace.repcap_trace_get()
	driver.trace.repcap_trace_set(repcap.Trace.Nr1)





.. autoclass:: RsSmab.Implementations.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trace_Freq.rst
	Trace_Pow.rst
	Trace_Power.rst
	Trace_Time.rst