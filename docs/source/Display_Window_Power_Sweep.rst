Sweep
----------------------------------------





.. autoclass:: RsSmab.Implementations.Display.Window.Power.Sweep.SweepCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.display.window.power.sweep.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Display_Window_Power_Sweep_Background.rst
	Display_Window_Power_Sweep_Grid.rst