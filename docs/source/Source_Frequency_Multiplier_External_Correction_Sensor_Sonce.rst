Sonce
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:CORRection:SENSor<CH>:SONCe

.. code-block:: python

	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:CORRection:SENSor<CH>:SONCe



.. autoclass:: RsSmab.Implementations.Source.Frequency.Multiplier.External.Correction.Sensor.Sonce.SonceCls
	:members:
	:undoc-members:
	:noindex: