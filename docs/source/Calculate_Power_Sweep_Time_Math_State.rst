State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:TIME:MATH<CH>:STATe

.. code-block:: python

	CALCulate:[POWer]:SWEep:TIME:MATH<CH>:STATe



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Time.Math.State.StateCls
	:members:
	:undoc-members:
	:noindex: