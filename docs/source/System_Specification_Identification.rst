Identification
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:SPECification:IDENtification:CATalog

.. code-block:: python

	SYSTem:SPECification:IDENtification:CATalog



.. autoclass:: RsSmab.Implementations.System.Specification.Identification.IdentificationCls
	:members:
	:undoc-members:
	:noindex: