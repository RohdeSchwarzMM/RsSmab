Offset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:FREQuency:[SENSor]:OFFSet

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:FREQuency:[SENSor]:OFFSet



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Frequency.Sensor.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.frequency.sensor.offset.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_Frequency_Sensor_Offset_State.rst