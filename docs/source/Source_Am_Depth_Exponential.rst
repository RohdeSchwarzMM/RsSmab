Exponential
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AM<CH>:DEPTh:EXPonential

.. code-block:: python

	[SOURce<HW>]:AM<CH>:DEPTh:EXPonential



.. autoclass:: RsSmab.Implementations.Source.Am.Depth.Exponential.ExponentialCls
	:members:
	:undoc-members:
	:noindex: