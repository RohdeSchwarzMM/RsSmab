Trigger
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:CHIRp:TRIGger:MODE

.. code-block:: python

	[SOURce<HW>]:CHIRp:TRIGger:MODE



.. autoclass:: RsSmab.Implementations.Source.Chirp.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.chirp.trigger.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Chirp_Trigger_Immediate.rst