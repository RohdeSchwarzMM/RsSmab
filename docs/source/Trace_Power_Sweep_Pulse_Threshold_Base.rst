Base
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:PULSe:THReshold:BASE

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:PULSe:THReshold:BASE



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Pulse.Threshold.Base.BaseCls
	:members:
	:undoc-members:
	:noindex: