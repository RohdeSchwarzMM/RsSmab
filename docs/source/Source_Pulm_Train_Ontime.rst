Ontime
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:PULM:TRAin:ONTime:POINts
	single: [SOURce<HW>]:PULM:TRAin:ONTime

.. code-block:: python

	[SOURce<HW>]:PULM:TRAin:ONTime:POINts
	[SOURce<HW>]:PULM:TRAin:ONTime



.. autoclass:: RsSmab.Implementations.Source.Pulm.Train.Ontime.OntimeCls
	:members:
	:undoc-members:
	:noindex: