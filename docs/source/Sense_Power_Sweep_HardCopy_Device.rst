Device
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:[POWer]:SWEep:HCOPy:DEVice:SIZE
	single: SENSe:[POWer]:SWEep:HCOPy:DEVice

.. code-block:: python

	SENSe:[POWer]:SWEep:HCOPy:DEVice:SIZE
	SENSe:[POWer]:SWEep:HCOPy:DEVice



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.HardCopy.Device.DeviceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.hardCopy.device.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_HardCopy_Device_Language.rst