Bb
----------------------------------------





.. autoclass:: RsSmab.Implementations.Source.Bb.BbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dme.rst
	Source_Bb_Ils.rst
	Source_Bb_Path.rst
	Source_Bb_Vor.rst