Marker
----------------------------------------





.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trace.power.sweep.measurement.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trace_Power_Sweep_Measurement_Marker_Display.rst