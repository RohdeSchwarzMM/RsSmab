Yval
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:FREQuency:MATH<CH>:YVAL

.. code-block:: python

	CALCulate:[POWer]:SWEep:FREQuency:MATH<CH>:YVAL



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Frequency.Math.Yval.YvalCls
	:members:
	:undoc-members:
	:noindex: