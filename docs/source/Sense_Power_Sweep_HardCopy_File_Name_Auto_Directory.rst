Directory
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:DIRectory:CLEar
	single: SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:DIRectory

.. code-block:: python

	SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:DIRectory:CLEar
	SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:DIRectory



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.HardCopy.File.Name.Auto.Directory.DirectoryCls
	:members:
	:undoc-members:
	:noindex: