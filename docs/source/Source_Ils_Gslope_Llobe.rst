Llobe
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:ILS:[GSLope]:LLOBe:[FREQuency]

.. code-block:: python

	[SOURce<HW>]:ILS:[GSLope]:LLOBe:[FREQuency]



.. autoclass:: RsSmab.Implementations.Source.Ils.Gslope.Llobe.LlobeCls
	:members:
	:undoc-members:
	:noindex: