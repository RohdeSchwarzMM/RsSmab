Month
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:[FILE]:MONTh:STATe
	single: SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:[FILE]:MONTh

.. code-block:: python

	SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:[FILE]:MONTh:STATe
	SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:[FILE]:MONTh



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.HardCopy.File.Name.Auto.File.Month.MonthCls
	:members:
	:undoc-members:
	:noindex: