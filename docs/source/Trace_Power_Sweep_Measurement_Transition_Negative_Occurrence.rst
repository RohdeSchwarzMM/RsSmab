Occurrence
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:MEASurement:TRANsition:NEGative:OCCurrence

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:MEASurement:TRANsition:NEGative:OCCurrence



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Transition.Negative.Occurrence.OccurrenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trace.power.sweep.measurement.transition.negative.occurrence.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trace_Power_Sweep_Measurement_Transition_Negative_Occurrence_Display.rst