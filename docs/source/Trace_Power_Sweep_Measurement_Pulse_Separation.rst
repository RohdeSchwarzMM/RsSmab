Separation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:MEASurement:PULSe:SEParation

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:MEASurement:PULSe:SEParation



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Pulse.Separation.SeparationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trace.power.sweep.measurement.pulse.separation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trace_Power_Sweep_Measurement_Pulse_Separation_Display.rst