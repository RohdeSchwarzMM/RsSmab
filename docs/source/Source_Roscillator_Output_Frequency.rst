Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:ROSCillator:OUTPut:FREQuency:MODE

.. code-block:: python

	[SOURce]:ROSCillator:OUTPut:FREQuency:MODE



.. autoclass:: RsSmab.Implementations.Source.Roscillator.Output.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: