Maximum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:MEASurement:POWer:MAXimum

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:MEASurement:POWer:MAXimum



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Power.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trace.power.sweep.measurement.power.maximum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trace_Power_Sweep_Measurement_Power_Maximum_Display.rst