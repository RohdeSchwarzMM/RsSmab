Stop
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:FREQuency:[SENSor]:SRANge:STOP

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:FREQuency:[SENSor]:SRANge:STOP



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Frequency.Sensor.Srange.Stop.StopCls
	:members:
	:undoc-members:
	:noindex: