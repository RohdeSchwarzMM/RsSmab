Code
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:[ILS]:MBEacon:COMid:CODE:STATe
	single: [SOURce<HW>]:[ILS]:MBEacon:COMid:CODE

.. code-block:: python

	[SOURce<HW>]:[ILS]:MBEacon:COMid:CODE:STATe
	[SOURce<HW>]:[ILS]:MBEacon:COMid:CODE



.. autoclass:: RsSmab.Implementations.Source.Ils.Mbeacon.Comid.Code.CodeCls
	:members:
	:undoc-members:
	:noindex: