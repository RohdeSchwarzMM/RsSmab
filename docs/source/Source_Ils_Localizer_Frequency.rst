Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:ILS:LOCalizer:FREQuency:MODE
	single: [SOURce<HW>]:ILS:LOCalizer:FREQuency:STEP
	single: [SOURce<HW>]:ILS:LOCalizer:FREQuency

.. code-block:: python

	[SOURce<HW>]:ILS:LOCalizer:FREQuency:MODE
	[SOURce<HW>]:ILS:LOCalizer:FREQuency:STEP
	[SOURce<HW>]:ILS:LOCalizer:FREQuency



.. autoclass:: RsSmab.Implementations.Source.Ils.Localizer.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: