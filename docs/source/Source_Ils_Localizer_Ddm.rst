Ddm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:ILS:LOCalizer:DDM:COUPling
	single: [SOURce<HW>]:ILS:LOCalizer:DDM:CURRent
	single: [SOURce<HW>]:ILS:LOCalizer:DDM:DIRection
	single: [SOURce<HW>]:ILS:LOCalizer:DDM:LOGarithmic
	single: [SOURce<HW>]:ILS:LOCalizer:DDM:PCT
	single: [SOURce<HW>]:ILS:LOCalizer:DDM:POLarity
	single: [SOURce<HW>]:ILS:LOCalizer:DDM:STEP
	single: [SOURce<HW>]:ILS:LOCalizer:DDM:[DEPTh]

.. code-block:: python

	[SOURce<HW>]:ILS:LOCalizer:DDM:COUPling
	[SOURce<HW>]:ILS:LOCalizer:DDM:CURRent
	[SOURce<HW>]:ILS:LOCalizer:DDM:DIRection
	[SOURce<HW>]:ILS:LOCalizer:DDM:LOGarithmic
	[SOURce<HW>]:ILS:LOCalizer:DDM:PCT
	[SOURce<HW>]:ILS:LOCalizer:DDM:POLarity
	[SOURce<HW>]:ILS:LOCalizer:DDM:STEP
	[SOURce<HW>]:ILS:LOCalizer:DDM:[DEPTh]



.. autoclass:: RsSmab.Implementations.Source.Ils.Localizer.Ddm.DdmCls
	:members:
	:undoc-members:
	:noindex: