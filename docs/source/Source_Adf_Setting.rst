Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:ADF:SETTing:CATalog
	single: [SOURce<HW>]:ADF:SETTing:DELete
	single: [SOURce<HW>]:ADF:SETTing:LOAD
	single: [SOURce<HW>]:ADF:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:ADF:SETTing:CATalog
	[SOURce<HW>]:ADF:SETTing:DELete
	[SOURce<HW>]:ADF:SETTing:LOAD
	[SOURce<HW>]:ADF:SETTing:STORe



.. autoclass:: RsSmab.Implementations.Source.Adf.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: