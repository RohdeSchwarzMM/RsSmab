State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:FREQuency:[SENSor]:OFFSet:STATe

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:FREQuency:[SENSor]:OFFSet:STATe



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Frequency.Sensor.Offset.State.StateCls
	:members:
	:undoc-members:
	:noindex: