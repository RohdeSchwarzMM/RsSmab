State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:SECurity:USBStorage:[STATe]

.. code-block:: python

	SYSTem:SECurity:USBStorage:[STATe]



.. autoclass:: RsSmab.Implementations.System.Security.UsbStorage.State.StateCls
	:members:
	:undoc-members:
	:noindex: