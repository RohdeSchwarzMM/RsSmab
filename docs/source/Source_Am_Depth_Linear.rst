Linear
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AM<CH>:DEPTh:LINear

.. code-block:: python

	[SOURce<HW>]:AM<CH>:DEPTh:LINear



.. autoclass:: RsSmab.Implementations.Source.Am.Depth.Linear.LinearCls
	:members:
	:undoc-members:
	:noindex: