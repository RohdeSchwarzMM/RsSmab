Code
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:[ILS]:LOCalizer:COMid:CODE:STATe
	single: [SOURce<HW>]:ILS:LOCalizer:COMid:CODE

.. code-block:: python

	[SOURce<HW>]:[ILS]:LOCalizer:COMid:CODE:STATe
	[SOURce<HW>]:ILS:LOCalizer:COMid:CODE



.. autoclass:: RsSmab.Implementations.Source.Ils.Localizer.Comid.Code.CodeCls
	:members:
	:undoc-members:
	:noindex: