State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:PROTect<CH>:[STATe]

.. code-block:: python

	SYSTem:PROTect<CH>:[STATe]



.. autoclass:: RsSmab.Implementations.System.Protect.State.StateCls
	:members:
	:undoc-members:
	:noindex: