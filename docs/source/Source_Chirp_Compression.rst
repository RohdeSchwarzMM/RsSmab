Compression
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:CHIRp:COMPression:RATio

.. code-block:: python

	[SOURce<HW>]:CHIRp:COMPression:RATio



.. autoclass:: RsSmab.Implementations.Source.Chirp.Compression.CompressionCls
	:members:
	:undoc-members:
	:noindex: