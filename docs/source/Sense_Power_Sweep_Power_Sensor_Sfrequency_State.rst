State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:POWer:[SENSor]:SFRequency:STATe

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:POWer:[SENSor]:SFRequency:STATe



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Power.Sensor.Sfrequency.State.StateCls
	:members:
	:undoc-members:
	:noindex: