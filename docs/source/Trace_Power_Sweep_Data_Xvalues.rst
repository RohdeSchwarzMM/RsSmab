Xvalues
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:DATA:XVALues

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:DATA:XVALues



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Data.Xvalues.XvaluesCls
	:members:
	:undoc-members:
	:noindex: