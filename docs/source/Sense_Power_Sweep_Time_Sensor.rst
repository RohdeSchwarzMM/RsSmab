Sensor
----------------------------------------





.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.Sensor.SensorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.time.sensor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_Time_Sensor_Offset.rst
	Sense_Power_Sweep_Time_Sensor_Pulse.rst
	Sense_Power_Sweep_Time_Sensor_Sfrequency.rst
	Sense_Power_Sweep_Time_Sensor_Trigger.rst