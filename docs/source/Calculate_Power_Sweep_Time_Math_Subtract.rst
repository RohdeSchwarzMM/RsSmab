Subtract
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:TIME:MATH<CH>:SUBTract

.. code-block:: python

	CALCulate:[POWer]:SWEep:TIME:MATH<CH>:SUBTract



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Time.Math.Subtract.SubtractCls
	:members:
	:undoc-members:
	:noindex: