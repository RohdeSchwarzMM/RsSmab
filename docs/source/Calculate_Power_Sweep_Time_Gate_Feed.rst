Feed
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:TIME:GATE<CH>:FEED

.. code-block:: python

	CALCulate:[POWer]:SWEep:TIME:GATE<CH>:FEED



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Time.Gate.Feed.FeedCls
	:members:
	:undoc-members:
	:noindex: