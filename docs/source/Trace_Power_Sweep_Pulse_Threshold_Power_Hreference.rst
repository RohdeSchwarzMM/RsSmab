Hreference
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:PULSe:THReshold:POWer:HREFerence

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:PULSe:THReshold:POWer:HREFerence



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Pulse.Threshold.Power.Hreference.HreferenceCls
	:members:
	:undoc-members:
	:noindex: