Vor
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:VOR:PRESet
	single: [SOURce<HW>]:BB:VOR:STATe

.. code-block:: python

	[SOURce<HW>]:BB:VOR:PRESet
	[SOURce<HW>]:BB:VOR:STATe



.. autoclass:: RsSmab.Implementations.Source.Bb.Vor.VorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.vor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Vor_Setting.rst