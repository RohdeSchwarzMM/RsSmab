Time
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:[POWer]:SWEep:TIME:RMODe
	single: SENSe:[POWer]:SWEep:TIME:STARt
	single: SENSe:[POWer]:SWEep:TIME:STEPs
	single: SENSe:[POWer]:SWEep:TIME:STOP
	single: SENSe:[POWer]:SWEep:TIME:TEVents

.. code-block:: python

	SENSe:[POWer]:SWEep:TIME:RMODe
	SENSe:[POWer]:SWEep:TIME:STARt
	SENSe:[POWer]:SWEep:TIME:STEPs
	SENSe:[POWer]:SWEep:TIME:STOP
	SENSe:[POWer]:SWEep:TIME:TEVents



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.TimeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.time.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_Time_Average.rst
	Sense_Power_Sweep_Time_Reference.rst
	Sense_Power_Sweep_Time_Sensor.rst
	Sense_Power_Sweep_Time_Spacing.rst
	Sense_Power_Sweep_Time_Yscale.rst