Source
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:LFOutput<CH>:SOURce

.. code-block:: python

	[SOURce]:LFOutput<CH>:SOURce



.. autoclass:: RsSmab.Implementations.Source.LfOutput.Source.SourceCls
	:members:
	:undoc-members:
	:noindex: