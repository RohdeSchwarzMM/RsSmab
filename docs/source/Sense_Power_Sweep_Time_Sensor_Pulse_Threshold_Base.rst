Base
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:PULSe:THReshold:BASE

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:PULSe:THReshold:BASE



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.Sensor.Pulse.Threshold.Base.BaseCls
	:members:
	:undoc-members:
	:noindex: