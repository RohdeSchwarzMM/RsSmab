Marker<Marker>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr0 .. Nr31
	rc = driver.source.sweep.frequency.marker.repcap_marker_get()
	driver.source.sweep.frequency.marker.repcap_marker_set(repcap.Marker.Nr0)



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:SWEep:[FREQuency]:MARKer:ACTive

.. code-block:: python

	[SOURce<HW>]:SWEep:[FREQuency]:MARKer:ACTive



.. autoclass:: RsSmab.Implementations.Source.Sweep.Frequency.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.sweep.frequency.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Sweep_Frequency_Marker_Frequency.rst
	Source_Sweep_Frequency_Marker_Fstate.rst