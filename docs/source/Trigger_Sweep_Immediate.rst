Immediate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger<HW>:[SWEep]:[IMMediate]

.. code-block:: python

	TRIGger<HW>:[SWEep]:[IMMediate]



.. autoclass:: RsSmab.Implementations.Trigger.Sweep.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex: