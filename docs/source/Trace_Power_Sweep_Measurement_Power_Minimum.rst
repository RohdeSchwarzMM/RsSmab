Minimum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:MEASurement:POWer:MINimum

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:MEASurement:POWer:MINimum



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Power.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trace.power.sweep.measurement.power.minimum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trace_Power_Sweep_Measurement_Power_Minimum_Display.rst