Points
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:DATA:POINts

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:DATA:POINts



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Data.Points.PointsCls
	:members:
	:undoc-members:
	:noindex: