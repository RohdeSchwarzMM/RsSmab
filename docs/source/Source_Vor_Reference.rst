Reference
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:VOR:REFerence:[DEViation]

.. code-block:: python

	[SOURce<HW>]:VOR:REFerence:[DEViation]



.. autoclass:: RsSmab.Implementations.Source.Vor.Reference.ReferenceCls
	:members:
	:undoc-members:
	:noindex: