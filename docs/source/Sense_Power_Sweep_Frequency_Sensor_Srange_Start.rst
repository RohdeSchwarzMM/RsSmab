Start
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:FREQuency:[SENSor]:SRANge:STARt

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:FREQuency:[SENSor]:SRANge:STARt



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Frequency.Sensor.Srange.Start.StartCls
	:members:
	:undoc-members:
	:noindex: