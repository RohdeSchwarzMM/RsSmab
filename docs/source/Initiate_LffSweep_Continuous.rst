Continuous
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: INITiate<HW>:LFFSweep:CONTinuous

.. code-block:: python

	INITiate<HW>:LFFSweep:CONTinuous



.. autoclass:: RsSmab.Implementations.Initiate.LffSweep.Continuous.ContinuousCls
	:members:
	:undoc-members:
	:noindex: