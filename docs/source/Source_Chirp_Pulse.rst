Pulse
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:CHIRp:PULSe:NUMBer
	single: [SOURce<HW>]:CHIRp:PULSe:PERiod
	single: [SOURce<HW>]:CHIRp:PULSe:WIDTh

.. code-block:: python

	[SOURce<HW>]:CHIRp:PULSe:NUMBer
	[SOURce<HW>]:CHIRp:PULSe:PERiod
	[SOURce<HW>]:CHIRp:PULSe:WIDTh



.. autoclass:: RsSmab.Implementations.Source.Chirp.Pulse.PulseCls
	:members:
	:undoc-members:
	:noindex: