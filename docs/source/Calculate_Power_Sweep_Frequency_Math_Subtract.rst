Subtract
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:FREQuency:MATH<CH>:SUBTract

.. code-block:: python

	CALCulate:[POWer]:SWEep:FREQuency:MATH<CH>:SUBTract



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Frequency.Math.Subtract.SubtractCls
	:members:
	:undoc-members:
	:noindex: