Src
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:TIME:SWEep:SRC

.. code-block:: python

	TRACe<CH>:TIME:SWEep:SRC



.. autoclass:: RsSmab.Implementations.Trace.Time.Sweep.Src.SrcCls
	:members:
	:undoc-members:
	:noindex: