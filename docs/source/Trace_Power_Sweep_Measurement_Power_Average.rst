Average
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:MEASurement:POWer:AVERage

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:MEASurement:POWer:AVERage



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Power.Average.AverageCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trace.power.sweep.measurement.power.average.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trace_Power_Sweep_Measurement_Power_Average_Display.rst