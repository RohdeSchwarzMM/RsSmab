Sweep
----------------------------------------





.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.SweepCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calculate.power.sweep.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calculate_Power_Sweep_Frequency.rst
	Calculate_Power_Sweep_Power.rst
	Calculate_Power_Sweep_Time.rst