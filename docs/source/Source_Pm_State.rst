State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:PM<CH>:STATe

.. code-block:: python

	[SOURce<HW>]:PM<CH>:STATe



.. autoclass:: RsSmab.Implementations.Source.Pm.State.StateCls
	:members:
	:undoc-members:
	:noindex: