Llobe
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:ILS:LOCalizer:LLOBe:[FREQuency]

.. code-block:: python

	[SOURce<HW>]:ILS:LOCalizer:LLOBe:[FREQuency]



.. autoclass:: RsSmab.Implementations.Source.Ils.Localizer.Llobe.LlobeCls
	:members:
	:undoc-members:
	:noindex: