Average
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:TIME:GATE<CH>:AVERage

.. code-block:: python

	CALCulate:[POWer]:SWEep:TIME:GATE<CH>:AVERage



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Time.Gate.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: