Pulse
----------------------------------------





.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.Sensor.Pulse.PulseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.time.sensor.pulse.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_Time_Sensor_Pulse_State.rst
	Sense_Power_Sweep_Time_Sensor_Pulse_Threshold.rst