Exponential
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AM<CH>:SENSitivity:EXPonential

.. code-block:: python

	[SOURce<HW>]:AM<CH>:SENSitivity:EXPonential



.. autoclass:: RsSmab.Implementations.Source.Am.Sensitivity.Exponential.ExponentialCls
	:members:
	:undoc-members:
	:noindex: