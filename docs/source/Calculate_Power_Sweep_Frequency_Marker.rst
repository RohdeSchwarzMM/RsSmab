Marker<Marker>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr0 .. Nr31
	rc = driver.calculate.power.sweep.frequency.marker.repcap_marker_get()
	driver.calculate.power.sweep.frequency.marker.repcap_marker_set(repcap.Marker.Nr0)





.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Frequency.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calculate.power.sweep.frequency.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calculate_Power_Sweep_Frequency_Marker_Feed.rst
	Calculate_Power_Sweep_Frequency_Marker_State.rst