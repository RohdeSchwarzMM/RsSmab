Information
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:INFormation:SR

.. code-block:: python

	SYSTem:INFormation:SR



.. autoclass:: RsSmab.Implementations.System.Information.InformationCls
	:members:
	:undoc-members:
	:noindex: