Yvalues
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:DATA:YVALues

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:DATA:YVALues



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Data.Yvalues.YvaluesCls
	:members:
	:undoc-members:
	:noindex: