State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:MEASurement:TRANsition:NEGative:OVERshoot:DISPlay:ANNotation:[STATe]

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:MEASurement:TRANsition:NEGative:OVERshoot:DISPlay:ANNotation:[STATe]



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Transition.Negative.Overshoot.Display.Annotation.State.StateCls
	:members:
	:undoc-members:
	:noindex: