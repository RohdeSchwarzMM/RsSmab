Annotation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe:[POWer]:SWEep:MEASurement:FULLscreen:DISPlay:ANNotation:[STATe]

.. code-block:: python

	TRACe:[POWer]:SWEep:MEASurement:FULLscreen:DISPlay:ANNotation:[STATe]



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Fullscreen.Display.Annotation.AnnotationCls
	:members:
	:undoc-members:
	:noindex: