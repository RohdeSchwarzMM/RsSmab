Linear
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AM<CH>:SENSitivity:[LINear]

.. code-block:: python

	[SOURce<HW>]:AM<CH>:SENSitivity:[LINear]



.. autoclass:: RsSmab.Implementations.Source.Am.Sensitivity.Linear.LinearCls
	:members:
	:undoc-members:
	:noindex: