Attenuation
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:POWer:ATTenuation:MAXLevel
	single: [SOURce<HW>]:POWer:ATTenuation:PATTenuator
	single: [SOURce<HW>]:POWer:ATTenuation:STAGe

.. code-block:: python

	[SOURce<HW>]:POWer:ATTenuation:MAXLevel
	[SOURce<HW>]:POWer:ATTenuation:PATTenuator
	[SOURce<HW>]:POWer:ATTenuation:STAGe



.. autoclass:: RsSmab.Implementations.Source.Power.Attenuation.AttenuationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.power.attenuation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Power_Attenuation_RfOff.rst