Period
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:LFOutput<CH>:SHAPe:TRIangle:PERiod

.. code-block:: python

	[SOURce<HW>]:LFOutput<CH>:SHAPe:TRIangle:PERiod



.. autoclass:: RsSmab.Implementations.Source.LfOutput.Shape.Triangle.Period.PeriodCls
	:members:
	:undoc-members:
	:noindex: