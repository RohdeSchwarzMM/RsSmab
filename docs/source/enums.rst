Enums
=========

AcDc
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AcDc.AC
	# All values (2x):
	AC | DC

AlcOffMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AlcOffMode.SHOLd
	# All values (2x):
	SHOLd | TABLe

AlcOnOffAuto
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.AlcOnOffAuto._0
	# Last value:
	value = enums.AlcOnOffAuto.PRESet
	# All values (9x):
	_0 | _1 | AUTO | OFF | OFFTable | ON | ONSample | ONTable
	PRESet

AmMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AmMode.NORMal
	# All values (2x):
	NORMal | SCAN

AmType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AmType.EXPonential
	# All values (2x):
	EXPonential | LINear

AutoManStep
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoManStep.AUTO
	# All values (3x):
	AUTO | MANual | STEP

AutoManualMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoManualMode.AUTO
	# All values (2x):
	AUTO | MANual

AutoStep
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoStep.AUTO
	# All values (2x):
	AUTO | STEP

AvionicCarrFreqMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AvionicCarrFreqMode.DECimal
	# All values (3x):
	DECimal | ICAO | USER

AvionicCarrFreqModeMrkBcn
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AvionicCarrFreqModeMrkBcn.PREDefined
	# All values (2x):
	PREDefined | USER

AvionicComIdTimeSchem
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AvionicComIdTimeSchem.STD
	# All values (2x):
	STD | USER

AvionicDdmStep
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AvionicDdmStep.DECimal
	# All values (2x):
	DECimal | PREDefined

AvionicExtAm
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AvionicExtAm.EXT
	# All values (2x):
	EXT | INT

AvionicIlsDdmCoup
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AvionicIlsDdmCoup.FIXed
	# All values (2x):
	FIXed | SDM

AvionicIlsDdmPol
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AvionicIlsDdmPol.P150_90
	# All values (2x):
	P150_90 | P90_150

AvionicIlsGsMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AvionicIlsGsMode.LLOBe
	# All values (3x):
	LLOBe | NORM | ULOBe

AvionicIlsIcaoChan
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.AvionicIlsIcaoChan.CH18X
	# Last value:
	value = enums.AvionicIlsIcaoChan.CH56Y
	# All values (40x):
	CH18X | CH18Y | CH20X | CH20Y | CH22X | CH22Y | CH24X | CH24Y
	CH26X | CH26Y | CH28X | CH28Y | CH30X | CH30Y | CH32X | CH32Y
	CH34X | CH34Y | CH36X | CH36Y | CH38X | CH38Y | CH40X | CH40Y
	CH42X | CH42Y | CH44X | CH44Y | CH46X | CH46Y | CH48X | CH48Y
	CH50X | CH50Y | CH52X | CH52Y | CH54X | CH54Y | CH56X | CH56Y

AvionicIlsLocMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AvionicIlsLocMode.LLOBe
	# All values (3x):
	LLOBe | NORM | RLOBe

AvionicIlsType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AvionicIlsType.GS
	# All values (4x):
	GS | GSLope | LOCalize | MBEacon

AvionicKnobStep
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AvionicKnobStep.DECimal
	# All values (2x):
	DECimal | ICAO

AvionicVorDir
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AvionicVorDir.FROM
	# All values (2x):
	FROM | TO

AvionicVorIcaoChan
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.AvionicVorIcaoChan.CH100X
	# Last value:
	value = enums.AvionicVorIcaoChan.CH99Y
	# All values (160x):
	CH100X | CH100Y | CH101X | CH101Y | CH102X | CH102Y | CH103X | CH103Y
	CH104X | CH104Y | CH105X | CH105Y | CH106X | CH106Y | CH107X | CH107Y
	CH108X | CH108Y | CH109X | CH109Y | CH110X | CH110Y | CH111X | CH111Y
	CH112X | CH112Y | CH113X | CH113Y | CH114X | CH114Y | CH115X | CH115Y
	CH116X | CH116Y | CH117X | CH117Y | CH118X | CH118Y | CH119X | CH119Y
	CH120X | CH120Y | CH121X | CH121Y | CH122X | CH122Y | CH123X | CH123Y
	CH124X | CH124Y | CH125X | CH125Y | CH126X | CH126Y | CH17X | CH17Y
	CH19X | CH19Y | CH21X | CH21Y | CH23X | CH23Y | CH25X | CH25Y
	CH27X | CH27Y | CH29X | CH29Y | CH31X | CH31Y | CH33X | CH33Y
	CH35X | CH35Y | CH37X | CH37Y | CH39X | CH39Y | CH41X | CH41Y
	CH43X | CH43Y | CH45X | CH45Y | CH47X | CH47Y | CH49X | CH49Y
	CH51X | CH51Y | CH53X | CH53Y | CH55X | CH55Y | CH57X | CH57Y
	CH58X | CH58Y | CH59X | CH59Y | CH70X | CH70Y | CH71X | CH71Y
	CH72X | CH72Y | CH73X | CH73Y | CH74X | CH74Y | CH75X | CH75Y
	CH76X | CH76Y | CH77X | CH77Y | CH78X | CH78Y | CH79X | CH79Y
	CH80X | CH80Y | CH81X | CH81Y | CH82X | CH82Y | CH83X | CH83Y
	CH84X | CH84Y | CH85X | CH85Y | CH86X | CH86Y | CH87X | CH87Y
	CH88X | CH88Y | CH89X | CH89Y | CH90X | CH90Y | CH91X | CH91Y
	CH92X | CH92Y | CH93X | CH93Y | CH94X | CH94Y | CH95X | CH95Y
	CH96X | CH96Y | CH97X | CH97Y | CH98X | CH98Y | CH99X | CH99Y

AvionicVorMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AvionicVorMode.FMSubcarrier
	# All values (4x):
	FMSubcarrier | NORM | SUBCarrier | VAR

ByteOrder
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ByteOrder.NORMal
	# All values (2x):
	NORMal | SWAPped

CalAdjMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CalAdjMode.BURNin
	# All values (2x):
	BURNin | FULL

CalDataMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CalDataMode.CUSTomer
	# All values (2x):
	CUSTomer | FACTory

CalDataUpdate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CalDataUpdate.BBFRC
	# All values (6x):
	BBFRC | FREQuency | IALL | LEVel | LEVForced | RFFRC

CalPowActorLinMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CalPowActorLinMode.AUTO
	# All values (2x):
	AUTO | OFF

CalPowAmpDetMode
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CalPowAmpDetMode.AMP
	# Last value:
	value = enums.CalPowAmpDetMode.OPU
	# All values (13x):
	AMP | AT20 | AT40 | AT6 | ATT | AUTO | FIXed | HP
	HP6 | OP20 | OP40 | OP6 | OPU

CalPowAttMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CalPowAttMode.NEW
	# All values (2x):
	NEW | OLD

CalPowBandwidth
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CalPowBandwidth.AUTO
	# All values (3x):
	AUTO | HIGH | LOW

CalPowDetLinMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CalPowDetLinMode.AUTO
	# All values (4x):
	AUTO | OFF | USER1 | USER2

CalPowOpuLconMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CalPowOpuLconMode.AM
	# All values (6x):
	AM | AUTO | CW | DAM | USER1 | USER2

ClkSynOutType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ClkSynOutType.CMOS
	# All values (4x):
	CMOS | DSINe | DSQuare | SESine

Colour
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Colour.GREen
	# All values (4x):
	GREen | NONE | RED | YELLow

DecimalSeparator
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DecimalSeparator.COMMa
	# All values (2x):
	COMMa | DOT

DevExpFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DevExpFormat.CGPRedefined
	# All values (4x):
	CGPRedefined | CGUSer | SCPI | XML

DexchExtension
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DexchExtension.CSV
	# All values (2x):
	CSV | TXT

DexchMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DexchMode.EXPort
	# All values (2x):
	EXPort | IMPort

DexchSepCol
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DexchSepCol.COMMa
	# All values (4x):
	COMMa | SEMicolon | SPACe | TABulator

DiagBgColor
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DiagBgColor.BLACk
	# All values (2x):
	BLACk | WHITe

DispKeybLockMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DispKeybLockMode.DISabled
	# All values (5x):
	DISabled | DONLy | ENABled | TOFF | VNConly

ErFpowSensMapping
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ErFpowSensMapping.SENS1
	# Last value:
	value = enums.ErFpowSensMapping.UNMapped
	# All values (9x):
	SENS1 | SENS2 | SENS3 | SENS4 | SENSor1 | SENSor2 | SENSor3 | SENSor4
	UNMapped

FilterWidth
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FilterWidth.NARRow
	# All values (2x):
	NARRow | WIDE

FmMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FmMode.HBANdwidth
	# All values (2x):
	HBANdwidth | LNOise

FmSour
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FmSour.EXT1
	# All values (7x):
	EXT1 | EXT2 | EXTernal | INTernal | LF1 | LF2 | NOISe

FormData
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FormData.ASCii
	# All values (2x):
	ASCii | PACKed

FormStatReg
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FormStatReg.ASCii
	# All values (4x):
	ASCii | BINary | HEXadecimal | OCTal

FreqMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FreqMode.COMBined
	# All values (5x):
	COMBined | CW | FIXed | LIST | SWEep

FreqPllModeF
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FreqPllModeF.NARRow
	# All values (2x):
	NARRow | NORMal

FreqStepMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FreqStepMode.DECimal
	# All values (2x):
	DECimal | USER

FreqSweepType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FreqSweepType.ANALog
	# All values (2x):
	ANALog | STEPped

FrontPanelLayout
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FrontPanelLayout.DIGits
	# All values (2x):
	DIGits | LETTers

HardCopyImageFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HardCopyImageFormat.BMP
	# All values (4x):
	BMP | JPG | PNG | XPM

HardCopyRegion
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HardCopyRegion.ALL
	# All values (2x):
	ALL | DIALog

HcopyDestination
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HcopyDestination.FILE
	# All values (2x):
	FILE | PRINter

IecDevId
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IecDevId.AUTO
	# All values (2x):
	AUTO | USER

IecTermMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IecTermMode.EOI
	# All values (2x):
	EOI | STANdard

Imp
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Imp.G50
	# All values (3x):
	G50 | G600 | HIGH

InclExcl
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InclExcl.EXCLude
	# All values (2x):
	EXCLude | INCLude

InpImpRf
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InpImpRf.G10K
	# All values (3x):
	G10K | G1K | G50

KbLayout
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.KbLayout.CHINese
	# Last value:
	value = enums.KbLayout.SWEDish
	# All values (20x):
	CHINese | DANish | DUTBe | DUTCh | ENGLish | ENGUK | ENGUS | FINNish
	FREBe | FRECa | FRENch | GERMan | ITALian | JAPanese | KORean | NORWegian
	PORTuguese | RUSSian | SPANish | SWEDish

LeftRightDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LeftRightDirection.LEFT
	# All values (2x):
	LEFT | RIGHt

LfBwidth
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LfBwidth.BW0M2
	# All values (2x):
	BW0M2 | BW10m

LfFreqMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LfFreqMode.CW
	# All values (3x):
	CW | FIXed | SWEep

LfShapeBfAmily
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LfShapeBfAmily.PULSe
	# All values (5x):
	PULSe | SINE | SQUare | TRAPeze | TRIangle

LfSource
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.LfSource.AM
	# Last value:
	value = enums.LfSource.NOISe
	# All values (17x):
	AM | AMA | AMB | EXT1 | EXT2 | FMPM | FMPMA | FMPMB
	LF1 | LF1A | LF1B | LF2 | LF2A | LF2B | NOISA | NOISB
	NOISe

LfSweepSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LfSweepSource.LF1
	# All values (2x):
	LF1 | LF2

LmodRunMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LmodRunMode.LEARned
	# All values (2x):
	LEARned | LIVE

LowHigh
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LowHigh.HIGH
	# All values (2x):
	HIGH | LOW

MeasRespHcOpCsvcLmSep
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasRespHcOpCsvcLmSep.BLANk
	# All values (4x):
	BLANk | COMMa | SEMicolon | TABulator

MeasRespHcOpCsvhEader
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasRespHcOpCsvhEader.OFF
	# All values (2x):
	OFF | STANdard

MeasRespHcOpCsvoRient
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasRespHcOpCsvoRient.HORizontal
	# All values (2x):
	HORizontal | VERTical

MeasRespHcOpFileFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasRespHcOpFileFormat.BMP
	# All values (5x):
	BMP | CSV | JPG | PNG | XPM

MeasRespMath
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.MeasRespMath.T1REf
	# Last value:
	value = enums.MeasRespMath.T4T4
	# All values (20x):
	T1REf | T1T1 | T1T2 | T1T3 | T1T4 | T2REf | T2T1 | T2T2
	T2T3 | T2T4 | T3REf | T3T1 | T3T2 | T3T3 | T3T4 | T4REf
	T4T1 | T4T2 | T4T3 | T4T4

MeasRespMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasRespMode.FREQuency
	# All values (3x):
	FREQuency | POWer | TIME

MeasRespPulsThrBase
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasRespPulsThrBase.POWer
	# All values (2x):
	POWer | VOLTage

MeasRespSpacingMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasRespSpacingMode.LINear
	# All values (2x):
	LINear | LOGarithmic

MeasRespTimeAverage
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.MeasRespTimeAverage._1
	# Last value:
	value = enums.MeasRespTimeAverage._8
	# All values (11x):
	_1 | _1024 | _128 | _16 | _2 | _256 | _32 | _4
	_512 | _64 | _8

MeasRespTimeGate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasRespTimeGate.TRAC1
	# All values (8x):
	TRAC1 | TRAC2 | TRAC3 | TRAC4 | TRACe1 | TRACe2 | TRACe3 | TRACe4

MeasRespTimingMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasRespTimingMode.FAST
	# All values (2x):
	FAST | NORMal

MeasRespTraceColor
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasRespTraceColor.BLUE
	# All values (7x):
	BLUE | GRAY | GREen | INVers | MAGenta | RED | YELLow

MeasRespTraceCopyDest
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasRespTraceCopyDest.REFerence
	# All values (1x):
	REFerence

MeasRespTraceFeed
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.MeasRespTraceFeed.NONE
	# Last value:
	value = enums.MeasRespTraceFeed.SENSor4
	# All values (10x):
	NONE | REFerence | SENS1 | SENS2 | SENS3 | SENS4 | SENSor1 | SENSor2
	SENSor3 | SENSor4

MeasRespTraceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasRespTraceState.HOLD
	# All values (3x):
	HOLD | OFF | ON

MeasRespTrigAutoSet
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasRespTrigAutoSet.ONCE
	# All values (1x):
	ONCE

MeasRespTrigMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasRespTrigMode.AUTO
	# All values (4x):
	AUTO | EXTernal | FREE | INTernal

MeasRespYsCaleEvents
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasRespYsCaleEvents.AND
	# All values (2x):
	AND | OR

MeasRespYsCaleMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasRespYsCaleMode.CEXPanding
	# All values (5x):
	CEXPanding | CFLoating | FEXPanding | FFLoating | OFF

ModulationDevMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModulationDevMode.RATio
	# All values (3x):
	RATio | TOTal | UNCoupled

NetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NetMode.AUTO
	# All values (2x):
	AUTO | STATic

NoisDistrib
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NoisDistrib.EQUal
	# All values (4x):
	EQUal | GAUSs | NORMal | UNIForm

NormalInverted
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NormalInverted.INVerted
	# All values (2x):
	INVerted | NORMal

ParameterSetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ParameterSetMode.GLOBal
	# All values (2x):
	GLOBal | LIST

Parity
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Parity.EVEN
	# All values (3x):
	EVEN | NONE | ODD

PixelTestPredefined
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PixelTestPredefined.AUTO
	# Last value:
	value = enums.PixelTestPredefined.WHITe
	# All values (9x):
	AUTO | BLACk | BLUE | GR25 | GR50 | GR75 | GREen | RED
	WHITe

PmMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PmMode.HBANdwidth
	# All values (3x):
	HBANdwidth | HDEViation | LNOise

PowAlcDetSensitivity
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowAlcDetSensitivity.AUTO
	# All values (5x):
	AUTO | FIXed | HIGH | LOW | MEDium

PowAlcStateWithExtAlc
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PowAlcStateWithExtAlc._0
	# Last value:
	value = enums.PowAlcStateWithExtAlc.PRESet
	# All values (10x):
	_0 | _1 | AUTO | EALC | OFF | OFFTable | ON | ONSample
	ONTable | PRESet

PowAttMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowAttMode.AUTO
	# All values (5x):
	AUTO | FIXed | HPOWer | MANual | NORMal

PowAttModeOut
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowAttModeOut.AUTO
	# All values (4x):
	AUTO | FIXed | HPOWer | NORMal

PowAttRfOffMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowAttRfOffMode.FATTenuation
	# All values (2x):
	FATTenuation | UNCHanged

PowAttStepArt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowAttStepArt.ELECtronic
	# All values (2x):
	ELECtronic | MECHanical

PowCntrlSelect
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowCntrlSelect.SENS1
	# All values (8x):
	SENS1 | SENS2 | SENS3 | SENS4 | SENSor1 | SENSor2 | SENSor3 | SENSor4

PowHarmMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowHarmMode._1
	# All values (3x):
	_1 | AUTO | ON

PowLevBehaviour
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowLevBehaviour.AUTO
	# All values (7x):
	AUTO | CPHase | CVSWr | HDUN | MONotone | UNINterrupted | USER

PowLevMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowLevMode.LOWDistortion
	# All values (3x):
	LOWDistortion | LOWNoise | NORMal

PowSensDisplayPriority
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowSensDisplayPriority.AVERage
	# All values (2x):
	AVERage | PEAK

PowSensFiltType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowSensFiltType.AUTO
	# All values (3x):
	AUTO | NSRatio | USER

PowSensSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowSensSource.A
	# All values (4x):
	A | B | RF | USER

PulsMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PulsMode.DOUBle
	# All values (4x):
	DOUBle | PHOPptrain | PTRain | SINGle

PulsTransType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PulsTransType.FAST
	# All values (2x):
	FAST | SMOothed

PulsTrigModeWithSingle
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PulsTrigModeWithSingle.AUTO
	# All values (5x):
	AUTO | EGATe | ESINgle | EXTernal | SINGle

RecScpiCmdMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RecScpiCmdMode.AUTO
	# All values (4x):
	AUTO | DAUTo | MANual | OFF

RepeatMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RepeatMode.CONTinuous
	# All values (2x):
	CONTinuous | SINGle

RfFreqMultCcorMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RfFreqMultCcorMode.HPRecision
	# All values (3x):
	HPRecision | NONE | STANdard

Rosc1GoUtpFreqMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Rosc1GoUtpFreqMode.DER1G
	# All values (3x):
	DER1G | LOOPthrough | OFF

RoscFreqExt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RoscFreqExt._100MHZ
	# All values (4x):
	_100MHZ | _10MHZ | _1GHZ | VARiable

RoscOutpFreqMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RoscOutpFreqMode.DER100M
	# All values (4x):
	DER100M | DER10M | LOOPthrough | OFF

Rs232BdRate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Rs232BdRate._115200
	# All values (7x):
	_115200 | _19200 | _2400 | _38400 | _4800 | _57600 | _9600

Rs232StopBits
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Rs232StopBits._1
	# All values (2x):
	_1 | _2

SelftLev
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SelftLev.CUSTomer
	# All values (3x):
	CUSTomer | PRODuction | SERVice

SelftLevWrite
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SelftLevWrite.CUSTomer
	# All values (4x):
	CUSTomer | NONE | PRODuction | SERVice

SelOutpMarkUser
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SelOutpMarkUser.MARK
	# All values (2x):
	MARK | USER

SelOutpVxAxis
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SelOutpVxAxis.S0V25
	# All values (4x):
	S0V25 | S0V5 | S1V0 | XAXis

SensorModeAll
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SensorModeAll.AUTO
	# All values (3x):
	AUTO | EXTSingle | SINGle

SingExtAuto
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SingExtAuto.AUTO
	# All values (8x):
	AUTO | BUS | DHOP | EAUTo | EXTernal | HOP | IMMediate | SINGle

SlopeType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SlopeType.NEGative
	# All values (2x):
	NEGative | POSitive

SourceInt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SourceInt.EXTernal
	# All values (2x):
	EXTernal | INTernal

Spacing
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Spacing.LINear
	# All values (3x):
	LINear | LOGarithmic | RAMP

StagMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StagMode.AUTO
	# All values (3x):
	AUTO | FIXed | USER

StateExtended
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StateExtended._0
	# All values (6x):
	_0 | _1 | _2 | DEFault | OFF | ON

SweCyclMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SweCyclMode.SAWTooth
	# All values (2x):
	SAWTooth | TRIangle

SweepType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SweepType.ADVanced
	# All values (2x):
	ADVanced | STANdard

SweMarkActive
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SweMarkActive.M01
	# Last value:
	value = enums.SweMarkActive.NONE
	# All values (11x):
	M01 | M02 | M03 | M04 | M05 | M06 | M07 | M08
	M09 | M10 | NONE

Test
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Test._0
	# All values (4x):
	_0 | _1 | RUNning | STOPped

TestCalSelected
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TestCalSelected._0
	# All values (2x):
	_0 | _1

TimeProtocol
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimeProtocol._0
	# All values (6x):
	_0 | _1 | NONE | NTP | OFF | ON

TraceSourceAll
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TraceSourceAll.HOLD
	# All values (8x):
	HOLD | OFF | ON | REF | SEN1 | SEN2 | SEN3 | SEN4

TrigSweepImmBusExt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TrigSweepImmBusExt.BUS
	# All values (3x):
	BUS | EXTernal | IMMediate

TrigSweepSourNoHopExtAuto
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TrigSweepSourNoHopExtAuto.AUTO
	# All values (5x):
	AUTO | BUS | EXTernal | IMMediate | SINGle

UnchOff
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnchOff.OFF
	# All values (2x):
	OFF | UNCHanged

UnitAngle
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitAngle.DEGree
	# All values (3x):
	DEGree | DEGRee | RADian

UnitPower
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitPower.DBM
	# All values (3x):
	DBM | DBUV | V

UnitPowSens
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitPowSens.DBM
	# All values (3x):
	DBM | DBUV | WATT

UnitSpeed
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitSpeed.KMH
	# All values (4x):
	KMH | MPH | MPS | NMPH

UpDownDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UpDownDirection.DOWN
	# All values (2x):
	DOWN | UP

UpdPolicyMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UpdPolicyMode.CONFirm
	# All values (3x):
	CONFirm | IGNore | STRict

