Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CSYNthesis:FREQuency

.. code-block:: python

	CSYNthesis:FREQuency



.. autoclass:: RsSmab.Implementations.Csynthesis.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.csynthesis.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Csynthesis_Frequency_Step.rst