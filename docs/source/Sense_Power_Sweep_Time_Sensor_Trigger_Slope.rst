Slope
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:TRIGger:SLOPe

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:TRIGger:SLOPe



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.Sensor.Trigger.Slope.SlopeCls
	:members:
	:undoc-members:
	:noindex: