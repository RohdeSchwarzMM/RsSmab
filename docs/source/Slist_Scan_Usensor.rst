Usensor
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SLISt:SCAN:USENsor

.. code-block:: python

	SLISt:SCAN:USENsor



.. autoclass:: RsSmab.Implementations.Slist.Scan.Usensor.UsensorCls
	:members:
	:undoc-members:
	:noindex: