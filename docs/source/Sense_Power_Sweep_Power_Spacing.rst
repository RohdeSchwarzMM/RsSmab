Spacing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:[POWer]:SWEep:POWer:SPACing:[MODE]

.. code-block:: python

	SENSe:[POWer]:SWEep:POWer:SPACing:[MODE]



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Power.Spacing.SpacingCls
	:members:
	:undoc-members:
	:noindex: