Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DME:SETTing:DELete
	single: [SOURce<HW>]:BB:DME:SETTing:LOAD
	single: [SOURce<HW>]:BB:DME:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:BB:DME:SETTing:DELete
	[SOURce<HW>]:BB:DME:SETTing:LOAD
	[SOURce<HW>]:BB:DME:SETTing:STORe



.. autoclass:: RsSmab.Implementations.Source.Bb.Dme.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: