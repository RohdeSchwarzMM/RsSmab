Maximum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:TIME:GATE<CH>:MAXimum

.. code-block:: python

	CALCulate:[POWer]:SWEep:TIME:GATE<CH>:MAXimum



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Time.Gate.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: