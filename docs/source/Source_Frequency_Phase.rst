Phase
----------------------------------------





.. autoclass:: RsSmab.Implementations.Source.Frequency.Phase.PhaseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.frequency.phase.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Frequency_Phase_Continuous.rst