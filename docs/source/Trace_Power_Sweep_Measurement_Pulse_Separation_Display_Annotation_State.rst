State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:MEASurement:PULSe:SEParation:DISPlay:ANNotation:[STATe]

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:MEASurement:PULSe:SEParation:DISPlay:ANNotation:[STATe]



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Pulse.Separation.Display.Annotation.State.StateCls
	:members:
	:undoc-members:
	:noindex: