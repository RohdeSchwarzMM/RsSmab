Bandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:LFOutput<CH>:BANDwidth

.. code-block:: python

	[SOURce]:LFOutput<CH>:BANDwidth



.. autoclass:: RsSmab.Implementations.Source.LfOutput.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: