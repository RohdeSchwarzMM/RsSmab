Edetector
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:POWer:ALC:EDETector:FACTor
	single: [SOURce<HW>]:POWer:ALC:EDETector:LEVel

.. code-block:: python

	[SOURce<HW>]:POWer:ALC:EDETector:FACTor
	[SOURce<HW>]:POWer:ALC:EDETector:LEVel



.. autoclass:: RsSmab.Implementations.Source.Power.Alc.Edetector.EdetectorCls
	:members:
	:undoc-members:
	:noindex: