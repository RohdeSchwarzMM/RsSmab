State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:TIME:MARKer<CH>:STATe

.. code-block:: python

	CALCulate:[POWer]:SWEep:TIME:MARKer<CH>:STATe



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Time.Marker.State.StateCls
	:members:
	:undoc-members:
	:noindex: