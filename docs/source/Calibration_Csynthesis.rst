Csynthesis
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALibration:CSYNthesis:[MEASure]

.. code-block:: python

	CALibration:CSYNthesis:[MEASure]



.. autoclass:: RsSmab.Implementations.Calibration.Csynthesis.CsynthesisCls
	:members:
	:undoc-members:
	:noindex: