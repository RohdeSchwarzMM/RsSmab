Language
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:[POWer]:SWEep:HCOPy:DEVice:LANGuage

.. code-block:: python

	SENSe:[POWer]:SWEep:HCOPy:DEVice:LANGuage



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.HardCopy.Device.Language.LanguageCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.hardCopy.device.language.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_HardCopy_Device_Language_Csv.rst