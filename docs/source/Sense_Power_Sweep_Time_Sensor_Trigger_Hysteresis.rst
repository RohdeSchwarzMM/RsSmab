Hysteresis
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:TRIGger:HYSTeresis

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:TRIGger:HYSTeresis



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.Sensor.Trigger.Hysteresis.HysteresisCls
	:members:
	:undoc-members:
	:noindex: