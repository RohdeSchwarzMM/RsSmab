Background
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DISPlay:[WINDow]:[POWer]:SWEep:BACKground:COLor

.. code-block:: python

	DISPlay:[WINDow]:[POWer]:SWEep:BACKground:COLor



.. autoclass:: RsSmab.Implementations.Display.Window.Power.Sweep.Background.BackgroundCls
	:members:
	:undoc-members:
	:noindex: