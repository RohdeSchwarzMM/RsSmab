Sweep
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:SWEep:GENeration
	single: [SOURce<HW>]:SWEep:RESet:[ALL]

.. code-block:: python

	[SOURce<HW>]:SWEep:GENeration
	[SOURce<HW>]:SWEep:RESet:[ALL]



.. autoclass:: RsSmab.Implementations.Source.Sweep.SweepCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.sweep.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Sweep_Combined.rst
	Source_Sweep_Frequency.rst
	Source_Sweep_Marker.rst
	Source_Sweep_Power.rst