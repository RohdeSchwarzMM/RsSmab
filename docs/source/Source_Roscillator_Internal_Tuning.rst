Tuning
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce]:ROSCillator:INTernal:TUNing:SLOPe
	single: [SOURce]:ROSCillator:INTernal:TUNing:[STATe]

.. code-block:: python

	[SOURce]:ROSCillator:INTernal:TUNing:SLOPe
	[SOURce]:ROSCillator:INTernal:TUNing:[STATe]



.. autoclass:: RsSmab.Implementations.Source.Roscillator.Internal.Tuning.TuningCls
	:members:
	:undoc-members:
	:noindex: