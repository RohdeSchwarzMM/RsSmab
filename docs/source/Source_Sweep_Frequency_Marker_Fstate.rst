Fstate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:SWEep:[FREQuency]:MARKer<CH>:FSTate

.. code-block:: python

	[SOURce<HW>]:SWEep:[FREQuency]:MARKer<CH>:FSTate



.. autoclass:: RsSmab.Implementations.Source.Sweep.Frequency.Marker.Fstate.FstateCls
	:members:
	:undoc-members:
	:noindex: