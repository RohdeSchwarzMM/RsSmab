Sweep
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SWEep:TYPE

.. code-block:: python

	SWEep:TYPE



.. autoclass:: RsSmab.Implementations.Sweep.SweepCls
	:members:
	:undoc-members:
	:noindex: