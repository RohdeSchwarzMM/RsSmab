Start
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:TIME:GATE<CH>:STARt

.. code-block:: python

	CALCulate:[POWer]:SWEep:TIME:GATE<CH>:STARt



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Time.Gate.Start.StartCls
	:members:
	:undoc-members:
	:noindex: