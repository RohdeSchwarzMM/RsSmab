Stop
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:TIME:GATE<CH>:STOP

.. code-block:: python

	CALCulate:[POWer]:SWEep:TIME:GATE<CH>:STOP



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Time.Gate.Stop.StopCls
	:members:
	:undoc-members:
	:noindex: