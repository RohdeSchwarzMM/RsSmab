State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:TIME:GATE<CH>:STATe

.. code-block:: python

	CALCulate:[POWer]:SWEep:TIME:GATE<CH>:STATe



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Time.Gate.State.StateCls
	:members:
	:undoc-members:
	:noindex: