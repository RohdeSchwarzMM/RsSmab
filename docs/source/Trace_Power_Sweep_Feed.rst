Feed
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:FEED

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:FEED



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Feed.FeedCls
	:members:
	:undoc-members:
	:noindex: