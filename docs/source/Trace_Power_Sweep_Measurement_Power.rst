Power
----------------------------------------





.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trace.power.sweep.measurement.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trace_Power_Sweep_Measurement_Power_Average.rst
	Trace_Power_Sweep_Measurement_Power_Hreference.rst
	Trace_Power_Sweep_Measurement_Power_Lreference.rst
	Trace_Power_Sweep_Measurement_Power_Maximum.rst
	Trace_Power_Sweep_Measurement_Power_Minimum.rst
	Trace_Power_Sweep_Measurement_Power_Pulse.rst
	Trace_Power_Sweep_Measurement_Power_Reference.rst