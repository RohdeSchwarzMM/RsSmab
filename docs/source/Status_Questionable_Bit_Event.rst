Event
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:QUEStionable:BIT<BITNR>:[EVENt]

.. code-block:: python

	STATus:QUEStionable:BIT<BITNR>:[EVENt]



.. autoclass:: RsSmab.Implementations.Status.Questionable.Bit.Event.EventCls
	:members:
	:undoc-members:
	:noindex: