Device
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:DEVice:ID

.. code-block:: python

	SYSTem:DEVice:ID



.. autoclass:: RsSmab.Implementations.System.Device.DeviceCls
	:members:
	:undoc-members:
	:noindex: