Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:SWEep:[FREQuency]:MARKer<CH>:FREQuency

.. code-block:: python

	[SOURce<HW>]:SWEep:[FREQuency]:MARKer<CH>:FREQuency



.. autoclass:: RsSmab.Implementations.Source.Sweep.Frequency.Marker.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: