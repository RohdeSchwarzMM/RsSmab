Sensitivity
----------------------------------------





.. autoclass:: RsSmab.Implementations.Source.Am.Sensitivity.SensitivityCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.am.sensitivity.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Am_Sensitivity_Exponential.rst
	Source_Am_Sensitivity_Linear.rst