Year
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:[FILE]:YEAR:STATe
	single: SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:[FILE]:YEAR

.. code-block:: python

	SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:[FILE]:YEAR:STATe
	SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:[FILE]:YEAR



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.HardCopy.File.Name.Auto.File.Year.YearCls
	:members:
	:undoc-members:
	:noindex: