Output
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:SWEep:MARKer:OUTPut:POLarity

.. code-block:: python

	[SOURce<HW>]:SWEep:MARKer:OUTPut:POLarity



.. autoclass:: RsSmab.Implementations.Source.Sweep.Marker.Output.OutputCls
	:members:
	:undoc-members:
	:noindex: