File
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:[FILE]:NUMBer
	single: SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:FILE

.. code-block:: python

	SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:[FILE]:NUMBer
	SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:FILE



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.HardCopy.File.Name.Auto.File.FileCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.hardCopy.file.name.auto.file.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_HardCopy_File_Name_Auto_File_Day.rst
	Sense_Power_Sweep_HardCopy_File_Name_Auto_File_Month.rst
	Sense_Power_Sweep_HardCopy_File_Name_Auto_File_Prefix.rst
	Sense_Power_Sweep_HardCopy_File_Name_Auto_File_Year.rst