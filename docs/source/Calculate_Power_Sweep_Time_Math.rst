Math<Math>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.calculate.power.sweep.time.math.repcap_math_get()
	driver.calculate.power.sweep.time.math.repcap_math_set(repcap.Math.Nr1)





.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Time.Math.MathCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calculate.power.sweep.time.math.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calculate_Power_Sweep_Time_Math_State.rst
	Calculate_Power_Sweep_Time_Math_Subtract.rst
	Calculate_Power_Sweep_Time_Math_Xval.rst
	Calculate_Power_Sweep_Time_Math_Yval.rst