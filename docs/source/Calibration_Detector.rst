Detector
----------------------------------------





.. autoclass:: RsSmab.Implementations.Calibration.Detector.DetectorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calibration.detector.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calibration_Detector_RfLevel.rst