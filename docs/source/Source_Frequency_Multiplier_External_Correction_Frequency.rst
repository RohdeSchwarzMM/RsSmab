Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:CORRection:FREQuency:POINts
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:CORRection:FREQuency

.. code-block:: python

	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:CORRection:FREQuency:POINts
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:CORRection:FREQuency



.. autoclass:: RsSmab.Implementations.Source.Frequency.Multiplier.External.Correction.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: