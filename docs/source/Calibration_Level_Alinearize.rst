Alinearize
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALibration:LEVel:ALINearize:MODE

.. code-block:: python

	CALibration:LEVel:ALINearize:MODE



.. autoclass:: RsSmab.Implementations.Calibration.Level.Alinearize.AlinearizeCls
	:members:
	:undoc-members:
	:noindex: