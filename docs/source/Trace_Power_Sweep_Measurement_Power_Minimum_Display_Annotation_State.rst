State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:MEASurement:POWer:MINimum:DISPlay:ANNotation:[STATe]

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:MEASurement:POWer:MINimum:DISPlay:ANNotation:[STATe]



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Power.Minimum.Display.Annotation.State.StateCls
	:members:
	:undoc-members:
	:noindex: