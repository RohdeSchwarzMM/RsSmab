State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:MEASurement:TRANsition:POSitive:OCCurrence:DISPlay:ANNotation:[STATe]

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:MEASurement:TRANsition:POSitive:OCCurrence:DISPlay:ANNotation:[STATe]



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Transition.Positive.Occurrence.Display.Annotation.State.StateCls
	:members:
	:undoc-members:
	:noindex: