Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:[ILS]:MBEacon:FREQuency:MODE
	single: [SOURce<HW>]:[ILS]:MBEacon:FREQuency

.. code-block:: python

	[SOURce<HW>]:[ILS]:MBEacon:FREQuency:MODE
	[SOURce<HW>]:[ILS]:MBEacon:FREQuency



.. autoclass:: RsSmab.Implementations.Source.Ils.Mbeacon.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: