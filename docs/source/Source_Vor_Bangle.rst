Bangle
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:VOR:[BANGle]:DIRection
	single: [SOURce<HW>]:VOR:[BANGle]

.. code-block:: python

	[SOURce<HW>]:VOR:[BANGle]:DIRection
	[SOURce<HW>]:VOR:[BANGle]



.. autoclass:: RsSmab.Implementations.Source.Vor.Bangle.BangleCls
	:members:
	:undoc-members:
	:noindex: