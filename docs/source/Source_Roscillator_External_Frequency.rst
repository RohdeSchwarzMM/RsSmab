Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce]:ROSCillator:EXTernal:FREQuency:VARiable
	single: [SOURce]:ROSCillator:EXTernal:FREQuency

.. code-block:: python

	[SOURce]:ROSCillator:EXTernal:FREQuency:VARiable
	[SOURce]:ROSCillator:EXTernal:FREQuency



.. autoclass:: RsSmab.Implementations.Source.Roscillator.External.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: