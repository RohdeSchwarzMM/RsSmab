Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:VOR:SETTing:DELete
	single: [SOURce<HW>]:BB:VOR:SETTing:LOAD
	single: [SOURce<HW>]:BB:VOR:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:BB:VOR:SETTing:DELete
	[SOURce<HW>]:BB:VOR:SETTing:LOAD
	[SOURce<HW>]:BB:VOR:SETTing:STORe



.. autoclass:: RsSmab.Implementations.Source.Bb.Vor.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: