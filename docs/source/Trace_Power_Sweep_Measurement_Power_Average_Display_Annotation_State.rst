State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:MEASurement:POWer:AVERage:DISPlay:ANNotation:[STATe]

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:MEASurement:POWer:AVERage:DISPlay:ANNotation:[STATe]



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Power.Average.Display.Annotation.State.StateCls
	:members:
	:undoc-members:
	:noindex: