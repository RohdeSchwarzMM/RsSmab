Base
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:MEASurement:POWer:PULSe:BASE

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:MEASurement:POWer:PULSe:BASE



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Power.Pulse.Base.BaseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trace.power.sweep.measurement.power.pulse.base.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trace_Power_Sweep_Measurement_Power_Pulse_Base_Display.rst