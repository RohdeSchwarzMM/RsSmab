Ddm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:ILS:[GS]:DDM:COUPling
	single: [SOURce<HW>]:ILS:[GS]:DDM:CURRent
	single: [SOURce<HW>]:ILS:[GS]:DDM:DIRection
	single: [SOURce<HW>]:ILS:[GS]:DDM:LOGarithmic
	single: [SOURce<HW>]:ILS:[GS]:DDM:PCT
	single: [SOURce<HW>]:ILS:[GS]:DDM:POLarity
	single: [SOURce<HW>]:ILS:[GS]:DDM:STEP
	single: [SOURce<HW>]:ILS:[GS]:DDM:[DEPTh]

.. code-block:: python

	[SOURce<HW>]:ILS:[GS]:DDM:COUPling
	[SOURce<HW>]:ILS:[GS]:DDM:CURRent
	[SOURce<HW>]:ILS:[GS]:DDM:DIRection
	[SOURce<HW>]:ILS:[GS]:DDM:LOGarithmic
	[SOURce<HW>]:ILS:[GS]:DDM:PCT
	[SOURce<HW>]:ILS:[GS]:DDM:POLarity
	[SOURce<HW>]:ILS:[GS]:DDM:STEP
	[SOURce<HW>]:ILS:[GS]:DDM:[DEPTh]



.. autoclass:: RsSmab.Implementations.Source.Ils.Gs.Ddm.DdmCls
	:members:
	:undoc-members:
	:noindex: