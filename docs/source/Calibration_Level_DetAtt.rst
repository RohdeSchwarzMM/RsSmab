DetAtt
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALibration:LEVel:DETatt:MODE

.. code-block:: python

	CALibration:LEVel:DETatt:MODE



.. autoclass:: RsSmab.Implementations.Calibration.Level.DetAtt.DetAttCls
	:members:
	:undoc-members:
	:noindex: