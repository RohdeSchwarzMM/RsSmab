Rise
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:LFOutput<CH>:SHAPe:TRAPeze:RISE

.. code-block:: python

	[SOURce<HW>]:LFOutput<CH>:SHAPe:TRAPeze:RISE



.. autoclass:: RsSmab.Implementations.Source.LfOutput.Shape.Trapeze.Rise.RiseCls
	:members:
	:undoc-members:
	:noindex: