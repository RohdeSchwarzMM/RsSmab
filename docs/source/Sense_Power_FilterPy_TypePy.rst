TypePy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:FILTer:TYPE

.. code-block:: python

	SENSe<CH>:[POWer]:FILTer:TYPE



.. autoclass:: RsSmab.Implementations.Sense.Power.FilterPy.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: