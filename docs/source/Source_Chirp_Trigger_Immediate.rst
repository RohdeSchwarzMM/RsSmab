Immediate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:CHIRp:TRIGger:IMMediate

.. code-block:: python

	[SOURce<HW>]:CHIRp:TRIGger:IMMediate



.. autoclass:: RsSmab.Implementations.Source.Chirp.Trigger.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex: