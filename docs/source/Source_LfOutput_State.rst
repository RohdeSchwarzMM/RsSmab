State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:LFOutput<CH>:[STATe]

.. code-block:: python

	[SOURce]:LFOutput<CH>:[STATe]



.. autoclass:: RsSmab.Implementations.Source.LfOutput.State.StateCls
	:members:
	:undoc-members:
	:noindex: