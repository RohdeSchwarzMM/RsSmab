Frequency
----------------------------------------





.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calculate.power.sweep.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calculate_Power_Sweep_Frequency_Marker.rst
	Calculate_Power_Sweep_Frequency_Math.rst