Generic
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:GENeric:MSG

.. code-block:: python

	SYSTem:GENeric:MSG



.. autoclass:: RsSmab.Implementations.System.Generic.GenericCls
	:members:
	:undoc-members:
	:noindex: