State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:FREQuency:[SENSor]:SRANge:[STATe]

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:FREQuency:[SENSor]:SRANge:[STATe]



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Frequency.Sensor.Srange.State.StateCls
	:members:
	:undoc-members:
	:noindex: