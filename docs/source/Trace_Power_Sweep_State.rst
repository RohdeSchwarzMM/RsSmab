State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:STATe

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:STATe



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.State.StateCls
	:members:
	:undoc-members:
	:noindex: