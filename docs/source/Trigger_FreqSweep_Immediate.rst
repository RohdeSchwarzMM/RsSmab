Immediate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger<HW>:FSWeep:[IMMediate]

.. code-block:: python

	TRIGger<HW>:FSWeep:[IMMediate]



.. autoclass:: RsSmab.Implementations.Trigger.FreqSweep.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex: