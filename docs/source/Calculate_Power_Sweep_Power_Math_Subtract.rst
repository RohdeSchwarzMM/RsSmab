Subtract
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:POWer:MATH<CH>:SUBTract

.. code-block:: python

	CALCulate:[POWer]:SWEep:POWer:MATH<CH>:SUBTract



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Power.Math.Subtract.SubtractCls
	:members:
	:undoc-members:
	:noindex: