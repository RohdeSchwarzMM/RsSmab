Xval
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:TIME:MATH<CH>:XVAL

.. code-block:: python

	CALCulate:[POWer]:SWEep:TIME:MATH<CH>:XVAL



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Time.Math.Xval.XvalCls
	:members:
	:undoc-members:
	:noindex: