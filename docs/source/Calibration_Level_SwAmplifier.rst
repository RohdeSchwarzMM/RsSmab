SwAmplifier
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALibration:LEVel:SWAMplifier:STATe

.. code-block:: python

	CALibration:LEVel:SWAMplifier:STATe



.. autoclass:: RsSmab.Implementations.Calibration.Level.SwAmplifier.SwAmplifierCls
	:members:
	:undoc-members:
	:noindex: