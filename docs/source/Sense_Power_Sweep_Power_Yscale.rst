Yscale
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:[POWer]:SWEep:POWer:YSCale:MAXimum
	single: SENSe:[POWer]:SWEep:POWer:YSCale:MINimum

.. code-block:: python

	SENSe:[POWer]:SWEep:POWer:YSCale:MAXimum
	SENSe:[POWer]:SWEep:POWer:YSCale:MINimum



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Power.Yscale.YscaleCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.power.yscale.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_Power_Yscale_Auto.rst