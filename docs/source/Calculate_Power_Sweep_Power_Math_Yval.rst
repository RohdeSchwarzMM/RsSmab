Yval
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:POWer:MATH<CH>:YVAL

.. code-block:: python

	CALCulate:[POWer]:SWEep:POWer:MATH<CH>:YVAL



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Power.Math.Yval.YvalCls
	:members:
	:undoc-members:
	:noindex: