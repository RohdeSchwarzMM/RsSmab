Button
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DISPlay:BUTTon:BRIGhtness

.. code-block:: python

	DISPlay:BUTTon:BRIGhtness



.. autoclass:: RsSmab.Implementations.Display.Button.ButtonCls
	:members:
	:undoc-members:
	:noindex: