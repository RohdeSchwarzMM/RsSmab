Reference
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:PULSe:THReshold:POWer:REFerence

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:PULSe:THReshold:POWer:REFerence



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Pulse.Threshold.Power.Reference.ReferenceCls
	:members:
	:undoc-members:
	:noindex: