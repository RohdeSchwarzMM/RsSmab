Phase
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CSYNthesis:PHASe

.. code-block:: python

	CSYNthesis:PHASe



.. autoclass:: RsSmab.Implementations.Csynthesis.Phase.PhaseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.csynthesis.phase.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Csynthesis_Phase_Reference.rst