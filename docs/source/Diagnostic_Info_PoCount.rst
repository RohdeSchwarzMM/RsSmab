PoCount
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic:INFO:POCount:SET
	single: DIAGnostic:INFO:POCount

.. code-block:: python

	DIAGnostic:INFO:POCount:SET
	DIAGnostic:INFO:POCount



.. autoclass:: RsSmab.Implementations.Diagnostic.Info.PoCount.PoCountCls
	:members:
	:undoc-members:
	:noindex: