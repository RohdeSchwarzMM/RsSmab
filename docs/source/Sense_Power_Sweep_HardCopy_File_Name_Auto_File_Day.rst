Day
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:[FILE]:DAY:STATe
	single: SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:[FILE]:DAY

.. code-block:: python

	SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:[FILE]:DAY:STATe
	SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:[FILE]:DAY



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.HardCopy.File.Name.Auto.File.Day.DayCls
	:members:
	:undoc-members:
	:noindex: