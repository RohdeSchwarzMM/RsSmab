YsValue
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:DATA:YSValue

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:DATA:YSValue



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Data.YsValue.YsValueCls
	:members:
	:undoc-members:
	:noindex: