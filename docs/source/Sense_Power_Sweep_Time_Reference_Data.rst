Data
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:[POWer]:SWEep:TIME:REFerence:DATA:COPY
	single: SENSe:[POWer]:SWEep:TIME:REFerence:DATA:POINts
	single: SENSe:[POWer]:SWEep:TIME:REFerence:DATA:XVALues
	single: SENSe:[POWer]:SWEep:TIME:REFerence:DATA:YVALues

.. code-block:: python

	SENSe:[POWer]:SWEep:TIME:REFerence:DATA:COPY
	SENSe:[POWer]:SWEep:TIME:REFerence:DATA:POINts
	SENSe:[POWer]:SWEep:TIME:REFerence:DATA:XVALues
	SENSe:[POWer]:SWEep:TIME:REFerence:DATA:YVALues



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.Reference.Data.DataCls
	:members:
	:undoc-members:
	:noindex: