Multiplier
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:FREQuency:MULTiplier

.. code-block:: python

	[SOURce<HW>]:FREQuency:MULTiplier



.. autoclass:: RsSmab.Implementations.Source.Frequency.Multiplier.MultiplierCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.frequency.multiplier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Frequency_Multiplier_External.rst