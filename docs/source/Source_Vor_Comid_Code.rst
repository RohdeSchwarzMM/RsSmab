Code
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:VOR:COMid:CODE:STATe
	single: [SOURce<HW>]:VOR:COMid:CODE

.. code-block:: python

	[SOURce<HW>]:VOR:COMid:CODE:STATe
	[SOURce<HW>]:VOR:COMid:CODE



.. autoclass:: RsSmab.Implementations.Source.Vor.Comid.Code.CodeCls
	:members:
	:undoc-members:
	:noindex: