State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:POWer:[SENSor]:OFFSet:STATe

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:POWer:[SENSor]:OFFSet:STATe



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Power.Sensor.Offset.State.StateCls
	:members:
	:undoc-members:
	:noindex: