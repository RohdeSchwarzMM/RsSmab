Data
----------------------------------------





.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Data.DataCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trace.power.sweep.data.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trace_Power_Sweep_Data_Points.rst
	Trace_Power_Sweep_Data_Xvalues.rst
	Trace_Power_Sweep_Data_YsValue.rst
	Trace_Power_Sweep_Data_Yvalues.rst