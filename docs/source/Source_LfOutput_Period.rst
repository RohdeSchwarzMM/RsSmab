Period
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:LFOutput<CH>:PERiod

.. code-block:: python

	[SOURce<HW>]:LFOutput<CH>:PERiod



.. autoclass:: RsSmab.Implementations.Source.LfOutput.Period.PeriodCls
	:members:
	:undoc-members:
	:noindex: