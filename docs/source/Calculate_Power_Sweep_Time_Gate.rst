Gate<Gate>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.calculate.power.sweep.time.gate.repcap_gate_get()
	driver.calculate.power.sweep.time.gate.repcap_gate_set(repcap.Gate.Nr1)





.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Time.Gate.GateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calculate.power.sweep.time.gate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calculate_Power_Sweep_Time_Gate_Average.rst
	Calculate_Power_Sweep_Time_Gate_Feed.rst
	Calculate_Power_Sweep_Time_Gate_Maximum.rst
	Calculate_Power_Sweep_Time_Gate_Start.rst
	Calculate_Power_Sweep_Time_Gate_State.rst
	Calculate_Power_Sweep_Time_Gate_Stop.rst