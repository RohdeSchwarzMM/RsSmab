Execute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:PULM:TRAin:DEXChange:EXECute

.. code-block:: python

	[SOURce<HW>]:PULM:TRAin:DEXChange:EXECute



.. autoclass:: RsSmab.Implementations.Source.Pulm.Train.Dexchange.Execute.ExecuteCls
	:members:
	:undoc-members:
	:noindex: