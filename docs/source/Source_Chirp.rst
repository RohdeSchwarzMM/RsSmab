Chirp
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:CHIRp:BANDwidth
	single: [SOURce<HW>]:CHIRp:DIRection
	single: [SOURce<HW>]:CHIRp:STATe

.. code-block:: python

	[SOURce<HW>]:CHIRp:BANDwidth
	[SOURce<HW>]:CHIRp:DIRection
	[SOURce<HW>]:CHIRp:STATe



.. autoclass:: RsSmab.Implementations.Source.Chirp.ChirpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.chirp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Chirp_Compression.rst
	Source_Chirp_Pulse.rst
	Source_Chirp_Test.rst
	Source_Chirp_Trigger.rst