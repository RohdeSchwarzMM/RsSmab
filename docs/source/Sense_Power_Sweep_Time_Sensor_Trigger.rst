Trigger
----------------------------------------





.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.Sensor.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.time.sensor.trigger.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_Time_Sensor_Trigger_Auto.rst
	Sense_Power_Sweep_Time_Sensor_Trigger_Dtime.rst
	Sense_Power_Sweep_Time_Sensor_Trigger_Hysteresis.rst
	Sense_Power_Sweep_Time_Sensor_Trigger_Level.rst
	Sense_Power_Sweep_Time_Sensor_Trigger_Slope.rst
	Sense_Power_Sweep_Time_Sensor_Trigger_Source.rst