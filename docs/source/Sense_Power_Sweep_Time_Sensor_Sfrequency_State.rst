State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:SFRequency:STATe

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:SFRequency:STATe



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.Sensor.Sfrequency.State.StateCls
	:members:
	:undoc-members:
	:noindex: