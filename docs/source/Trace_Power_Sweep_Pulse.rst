Pulse
----------------------------------------





.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Pulse.PulseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trace.power.sweep.pulse.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trace_Power_Sweep_Pulse_Threshold.rst