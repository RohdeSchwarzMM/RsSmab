Usb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:USB:RESource

.. code-block:: python

	SYSTem:COMMunicate:USB:RESource



.. autoclass:: RsSmab.Implementations.System.Communicate.Usb.UsbCls
	:members:
	:undoc-members:
	:noindex: