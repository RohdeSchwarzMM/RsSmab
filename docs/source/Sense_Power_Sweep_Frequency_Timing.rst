Timing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:[POWer]:SWEep:FREQuency:TIMing:[MODE]

.. code-block:: python

	SENSe:[POWer]:SWEep:FREQuency:TIMing:[MODE]



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Frequency.Timing.TimingCls
	:members:
	:undoc-members:
	:noindex: