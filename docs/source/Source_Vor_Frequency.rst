Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:VOR:FREQuency:MODE
	single: [SOURce<HW>]:VOR:FREQuency:STEP
	single: [SOURce<HW>]:VOR:FREQuency

.. code-block:: python

	[SOURce<HW>]:VOR:FREQuency:MODE
	[SOURce<HW>]:VOR:FREQuency:STEP
	[SOURce<HW>]:VOR:FREQuency



.. autoclass:: RsSmab.Implementations.Source.Vor.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: