Feed
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:FREQuency:MARKer<CH>:FEED

.. code-block:: python

	CALCulate:[POWer]:SWEep:FREQuency:MARKer<CH>:FEED



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Frequency.Marker.Feed.FeedCls
	:members:
	:undoc-members:
	:noindex: