Duration
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:MEASurement:PULSe:DURation

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:MEASurement:PULSe:DURation



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Pulse.Duration.DurationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trace.power.sweep.measurement.pulse.duration.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trace_Power_Sweep_Measurement_Pulse_Duration_Display.rst