Spacing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:SWEep:POWer:SPACing:MODE

.. code-block:: python

	[SOURce<HW>]:SWEep:POWer:SPACing:MODE



.. autoclass:: RsSmab.Implementations.Source.Sweep.Power.Spacing.SpacingCls
	:members:
	:undoc-members:
	:noindex: