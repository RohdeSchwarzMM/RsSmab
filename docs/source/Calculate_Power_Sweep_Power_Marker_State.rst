State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:POWer:MARKer<CH>:STATe

.. code-block:: python

	CALCulate:[POWer]:SWEep:POWer:MARKer<CH>:STATe



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Power.Marker.State.StateCls
	:members:
	:undoc-members:
	:noindex: