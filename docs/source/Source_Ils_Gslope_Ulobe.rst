Ulobe
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:ILS:[GSLope]:ULOBe:[FREQuency]

.. code-block:: python

	[SOURce<HW>]:ILS:[GSLope]:ULOBe:[FREQuency]



.. autoclass:: RsSmab.Implementations.Source.Ils.Gslope.Ulobe.UlobeCls
	:members:
	:undoc-members:
	:noindex: