Data
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:[POWer]:SWEep:FREQuency:REFerence:DATA:COPY
	single: SENSe:[POWer]:SWEep:FREQuency:REFerence:DATA:POINts
	single: SENSe:[POWer]:SWEep:FREQuency:REFerence:DATA:XVALues
	single: SENSe:[POWer]:SWEep:FREQuency:REFerence:DATA:YVALues

.. code-block:: python

	SENSe:[POWer]:SWEep:FREQuency:REFerence:DATA:COPY
	SENSe:[POWer]:SWEep:FREQuency:REFerence:DATA:POINts
	SENSe:[POWer]:SWEep:FREQuency:REFerence:DATA:XVALues
	SENSe:[POWer]:SWEep:FREQuency:REFerence:DATA:YVALues



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Frequency.Reference.Data.DataCls
	:members:
	:undoc-members:
	:noindex: