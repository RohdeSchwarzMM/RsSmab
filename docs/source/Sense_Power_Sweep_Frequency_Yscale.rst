Yscale
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:[POWer]:SWEep:FREQuency:YSCale:MAXimum
	single: SENSe:[POWer]:SWEep:FREQuency:YSCale:MINimum

.. code-block:: python

	SENSe:[POWer]:SWEep:FREQuency:YSCale:MAXimum
	SENSe:[POWer]:SWEep:FREQuency:YSCale:MINimum



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Frequency.Yscale.YscaleCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.frequency.yscale.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_Frequency_Yscale_Auto.rst