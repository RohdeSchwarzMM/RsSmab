Yscale
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:[POWer]:SWEep:TIME:YSCale:MAXimum
	single: SENSe:[POWer]:SWEep:TIME:YSCale:MINimum

.. code-block:: python

	SENSe:[POWer]:SWEep:TIME:YSCale:MAXimum
	SENSe:[POWer]:SWEep:TIME:YSCale:MINimum



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.Yscale.YscaleCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.time.yscale.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_Time_Yscale_Auto.rst