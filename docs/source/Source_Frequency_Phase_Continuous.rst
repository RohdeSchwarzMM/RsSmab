Continuous
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:FREQuency:PHASe:CONTinuous:HIGH
	single: [SOURce<HW>]:FREQuency:PHASe:CONTinuous:LOW
	single: [SOURce<HW>]:FREQuency:PHASe:CONTinuous:MODE
	single: [SOURce<HW>]:FREQuency:PHASe:CONTinuous:STATe

.. code-block:: python

	[SOURce<HW>]:FREQuency:PHASe:CONTinuous:HIGH
	[SOURce<HW>]:FREQuency:PHASe:CONTinuous:LOW
	[SOURce<HW>]:FREQuency:PHASe:CONTinuous:MODE
	[SOURce<HW>]:FREQuency:PHASe:CONTinuous:STATe



.. autoclass:: RsSmab.Implementations.Source.Frequency.Phase.Continuous.ContinuousCls
	:members:
	:undoc-members:
	:noindex: