State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:OFFSet:STATe

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:OFFSet:STATe



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.Sensor.Offset.State.StateCls
	:members:
	:undoc-members:
	:noindex: