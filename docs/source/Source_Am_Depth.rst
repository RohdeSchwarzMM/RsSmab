Depth
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AM:DEPTh:SUM
	single: [SOURce<HW>]:AM<CH>:[DEPTh]

.. code-block:: python

	[SOURce<HW>]:AM:DEPTh:SUM
	[SOURce<HW>]:AM<CH>:[DEPTh]



.. autoclass:: RsSmab.Implementations.Source.Am.Depth.DepthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.am.depth.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Am_Depth_Exponential.rst
	Source_Am_Depth_Linear.rst