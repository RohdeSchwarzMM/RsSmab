State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:MEASurement:TRANsition:NEGative:OCCurrence:DISPlay:ANNotation:[STATe]

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:MEASurement:TRANsition:NEGative:OCCurrence:DISPlay:ANNotation:[STATe]



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Transition.Negative.Occurrence.Display.Annotation.State.StateCls
	:members:
	:undoc-members:
	:noindex: