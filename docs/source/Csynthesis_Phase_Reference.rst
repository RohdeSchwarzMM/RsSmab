Reference
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CSYNthesis:PHASe:REFerence

.. code-block:: python

	CSYNthesis:PHASe:REFerence



.. autoclass:: RsSmab.Implementations.Csynthesis.Phase.Reference.ReferenceCls
	:members:
	:undoc-members:
	:noindex: