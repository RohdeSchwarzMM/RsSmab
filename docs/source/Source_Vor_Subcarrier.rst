Subcarrier
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:VOR:SUBCarrier:DEPTh
	single: [SOURce<HW>]:VOR:SUBCarrier:[FREQuency]

.. code-block:: python

	[SOURce<HW>]:VOR:SUBCarrier:DEPTh
	[SOURce<HW>]:VOR:SUBCarrier:[FREQuency]



.. autoclass:: RsSmab.Implementations.Source.Vor.Subcarrier.SubcarrierCls
	:members:
	:undoc-members:
	:noindex: