Pulse
----------------------------------------





.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Power.Pulse.PulseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trace.power.sweep.measurement.power.pulse.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trace_Power_Sweep_Measurement_Power_Pulse_Base.rst
	Trace_Power_Sweep_Measurement_Power_Pulse_Top.rst