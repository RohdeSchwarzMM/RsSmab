Vor
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:VOR:MODE
	single: [SOURce<HW>]:VOR:PRESet
	single: [SOURce<HW>]:VOR:SOURce
	single: [SOURce<HW>]:VOR:STATe

.. code-block:: python

	[SOURce<HW>]:VOR:MODE
	[SOURce<HW>]:VOR:PRESet
	[SOURce<HW>]:VOR:SOURce
	[SOURce<HW>]:VOR:STATe



.. autoclass:: RsSmab.Implementations.Source.Vor.VorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.vor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Vor_Bangle.rst
	Source_Vor_Comid.rst
	Source_Vor_Frequency.rst
	Source_Vor_Icao.rst
	Source_Vor_Reference.rst
	Source_Vor_Setting.rst
	Source_Vor_Subcarrier.rst
	Source_Vor_Var.rst