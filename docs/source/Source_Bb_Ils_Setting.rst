Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:[BB]:ILS:SETTing:DELete
	single: [SOURce<HW>]:[BB]:ILS:SETTing:LOAD
	single: [SOURce<HW>]:[BB]:ILS:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:[BB]:ILS:SETTing:DELete
	[SOURce<HW>]:[BB]:ILS:SETTing:LOAD
	[SOURce<HW>]:[BB]:ILS:SETTing:STORe



.. autoclass:: RsSmab.Implementations.Source.Bb.Ils.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: