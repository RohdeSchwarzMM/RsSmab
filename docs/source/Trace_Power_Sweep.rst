Sweep
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:COPY

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:COPY



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.SweepCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trace.power.sweep.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trace_Power_Sweep_Color.rst
	Trace_Power_Sweep_Data.rst
	Trace_Power_Sweep_Feed.rst
	Trace_Power_Sweep_Measurement.rst
	Trace_Power_Sweep_Pulse.rst
	Trace_Power_Sweep_State.rst