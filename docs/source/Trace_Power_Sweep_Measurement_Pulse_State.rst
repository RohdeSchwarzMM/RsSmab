State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:MEASurement:PULSe:STATe

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:MEASurement:PULSe:STATe



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Pulse.State.StateCls
	:members:
	:undoc-members:
	:noindex: