Continuous
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: INITiate<HW>:[POWer]:CONTinuous

.. code-block:: python

	INITiate<HW>:[POWer]:CONTinuous



.. autoclass:: RsSmab.Implementations.Initiate.Power.Continuous.ContinuousCls
	:members:
	:undoc-members:
	:noindex: