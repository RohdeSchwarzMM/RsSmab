Name
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]

.. code-block:: python

	SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.HardCopy.File.Name.NameCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.hardCopy.file.name.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_HardCopy_File_Name_Auto.rst