Xval
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:FREQuency:MATH<CH>:XVAL

.. code-block:: python

	CALCulate:[POWer]:SWEep:FREQuency:MATH<CH>:XVAL



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Frequency.Math.Xval.XvalCls
	:members:
	:undoc-members:
	:noindex: