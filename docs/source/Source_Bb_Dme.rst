Dme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DME:PRESet
	single: [SOURce<HW>]:BB:DME:STATe

.. code-block:: python

	[SOURce<HW>]:BB:DME:PRESet
	[SOURce<HW>]:BB:DME:STATe



.. autoclass:: RsSmab.Implementations.Source.Bb.Dme.DmeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.dme.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dme_Gaussian.rst
	Source_Bb_Dme_Setting.rst