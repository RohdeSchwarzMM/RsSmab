Csynthesis
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CSYNthesis:OTYPe
	single: CSYNthesis:STATe
	single: CSYNthesis:VOLTage

.. code-block:: python

	CSYNthesis:OTYPe
	CSYNthesis:STATe
	CSYNthesis:VOLTage



.. autoclass:: RsSmab.Implementations.Csynthesis.CsynthesisCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.csynthesis.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Csynthesis_Frequency.rst
	Csynthesis_Offset.rst
	Csynthesis_Phase.rst
	Csynthesis_Power.rst