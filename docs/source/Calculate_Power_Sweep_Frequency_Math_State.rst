State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:FREQuency:MATH<CH>:STATe

.. code-block:: python

	CALCulate:[POWer]:SWEep:FREQuency:MATH<CH>:STATe



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Frequency.Math.State.StateCls
	:members:
	:undoc-members:
	:noindex: