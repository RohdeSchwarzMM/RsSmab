Auto
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:TRIGger:AUTO

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:TRIGger:AUTO



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.Sensor.Trigger.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex: