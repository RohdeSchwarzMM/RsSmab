Sfrequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:SFRequency

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:SFRequency



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.Sensor.Sfrequency.SfrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.time.sensor.sfrequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_Time_Sensor_Sfrequency_State.rst