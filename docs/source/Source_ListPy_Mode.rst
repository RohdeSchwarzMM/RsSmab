Mode
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:LIST:MODE:ADVanced
	single: [SOURce<HW>]:LIST:MODE

.. code-block:: python

	[SOURce<HW>]:LIST:MODE:ADVanced
	[SOURce<HW>]:LIST:MODE



.. autoclass:: RsSmab.Implementations.Source.ListPy.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: