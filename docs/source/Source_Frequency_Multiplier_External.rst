External
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:DAC0
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:DAC1
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:FMAXimum
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:FMINimum
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:IPMax
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:IPOWer
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:MULTiplier
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:PADJust
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:PMAXimum
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:PMINimum
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:PSDMinimum
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:REVision
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:SNUMber
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:STATe
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:TYPE

.. code-block:: python

	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:DAC0
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:DAC1
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:FMAXimum
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:FMINimum
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:IPMax
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:IPOWer
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:MULTiplier
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:PADJust
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:PMAXimum
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:PMINimum
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:PSDMinimum
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:REVision
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:SNUMber
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:STATe
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:TYPE



.. autoclass:: RsSmab.Implementations.Source.Frequency.Multiplier.External.ExternalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.frequency.multiplier.external.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Frequency_Multiplier_External_Correction.rst
	Source_Frequency_Multiplier_External_Firmware.rst
	Source_Frequency_Multiplier_External_Loader.rst