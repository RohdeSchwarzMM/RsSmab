State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:POWer:MATH<CH>:STATe

.. code-block:: python

	CALCulate:[POWer]:SWEep:POWer:MATH<CH>:STATe



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Power.Math.State.StateCls
	:members:
	:undoc-members:
	:noindex: