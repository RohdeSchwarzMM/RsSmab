Gs
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:ILS:GS:PRESet
	single: [SOURce<HW>]:ILS:GS:STATe
	single: [SOURce<HW>]:ILS:[GS]:MODE
	single: [SOURce<HW>]:ILS:[GS]:PHASe
	single: [SOURce<HW>]:ILS:[GS]:SDM
	single: [SOURce<HW>]:ILS:[GS]:SOURce

.. code-block:: python

	[SOURce<HW>]:ILS:GS:PRESet
	[SOURce<HW>]:ILS:GS:STATe
	[SOURce<HW>]:ILS:[GS]:MODE
	[SOURce<HW>]:ILS:[GS]:PHASe
	[SOURce<HW>]:ILS:[GS]:SDM
	[SOURce<HW>]:ILS:[GS]:SOURce



.. autoclass:: RsSmab.Implementations.Source.Ils.Gs.GsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.ils.gs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Ils_Gs_Ddm.rst
	Source_Ils_Gs_Frequency.rst
	Source_Ils_Gs_Icao.rst
	Source_Ils_Gs_Llobe.rst
	Source_Ils_Gs_Ulobe.rst