Step
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CSYNthesis:FREQuency:STEP:MODE
	single: CSYNthesis:FREQuency:STEP

.. code-block:: python

	CSYNthesis:FREQuency:STEP:MODE
	CSYNthesis:FREQuency:STEP



.. autoclass:: RsSmab.Implementations.Csynthesis.Frequency.Step.StepCls
	:members:
	:undoc-members:
	:noindex: