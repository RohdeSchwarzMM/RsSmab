Auto
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:STATe
	single: SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO

.. code-block:: python

	SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:STATe
	SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.HardCopy.File.Name.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.hardCopy.file.name.auto.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_HardCopy_File_Name_Auto_Directory.rst
	Sense_Power_Sweep_HardCopy_File_Name_Auto_File.rst