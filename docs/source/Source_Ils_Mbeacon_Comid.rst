Comid
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:[ILS]:MBEacon:COMid:DASH
	single: [SOURce<HW>]:[ILS]:MBEacon:COMid:DEPTh
	single: [SOURce<HW>]:[ILS]:MBEacon:COMid:DOT
	single: [SOURce<HW>]:[ILS]:MBEacon:COMid:FREQuency
	single: [SOURce<HW>]:[ILS]:MBEacon:COMid:LETTer
	single: [SOURce<HW>]:[ILS]:MBEacon:COMid:PERiod
	single: [SOURce<HW>]:[ILS]:MBEacon:COMid:SYMBol
	single: [SOURce<HW>]:[ILS]:MBEacon:COMid:TSCHema
	single: [SOURce<HW>]:[ILS]:MBEacon:COMid:[STATe]

.. code-block:: python

	[SOURce<HW>]:[ILS]:MBEacon:COMid:DASH
	[SOURce<HW>]:[ILS]:MBEacon:COMid:DEPTh
	[SOURce<HW>]:[ILS]:MBEacon:COMid:DOT
	[SOURce<HW>]:[ILS]:MBEacon:COMid:FREQuency
	[SOURce<HW>]:[ILS]:MBEacon:COMid:LETTer
	[SOURce<HW>]:[ILS]:MBEacon:COMid:PERiod
	[SOURce<HW>]:[ILS]:MBEacon:COMid:SYMBol
	[SOURce<HW>]:[ILS]:MBEacon:COMid:TSCHema
	[SOURce<HW>]:[ILS]:MBEacon:COMid:[STATe]



.. autoclass:: RsSmab.Implementations.Source.Ils.Mbeacon.Comid.ComidCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.ils.mbeacon.comid.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Ils_Mbeacon_Comid_Code.rst