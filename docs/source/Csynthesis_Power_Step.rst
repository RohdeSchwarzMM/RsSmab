Step
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CSYNthesis:POWer:STEP:MODE
	single: CSYNthesis:POWer:STEP:[INCRement]

.. code-block:: python

	CSYNthesis:POWer:STEP:MODE
	CSYNthesis:POWer:STEP:[INCRement]



.. autoclass:: RsSmab.Implementations.Csynthesis.Power.Step.StepCls
	:members:
	:undoc-members:
	:noindex: