Xval
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:POWer:MATH<CH>:XVAL

.. code-block:: python

	CALCulate:[POWer]:SWEep:POWer:MATH<CH>:XVAL



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Power.Math.Xval.XvalCls
	:members:
	:undoc-members:
	:noindex: