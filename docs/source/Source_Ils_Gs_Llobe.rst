Llobe
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:ILS:[GS]:LLOBe:[FREQuency]

.. code-block:: python

	[SOURce<HW>]:ILS:[GS]:LLOBe:[FREQuency]



.. autoclass:: RsSmab.Implementations.Source.Ils.Gs.Llobe.LlobeCls
	:members:
	:undoc-members:
	:noindex: