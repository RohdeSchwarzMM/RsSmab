File
----------------------------------------





.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.HardCopy.File.FileCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.hardCopy.file.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_HardCopy_File_Name.rst