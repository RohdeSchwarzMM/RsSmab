Ddm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:ILS:[GSLope]:DDM:COUPling
	single: [SOURce<HW>]:ILS:[GSLope]:DDM:CURRent
	single: [SOURce<HW>]:ILS:[GSLope]:DDM:DIRection
	single: [SOURce<HW>]:ILS:[GSLope]:DDM:LOGarithmic
	single: [SOURce<HW>]:ILS:[GSLope]:DDM:PCT
	single: [SOURce<HW>]:ILS:[GSLope]:DDM:POLarity
	single: [SOURce<HW>]:ILS:[GSLope]:DDM:STEP
	single: [SOURce<HW>]:ILS:[GSLope]:DDM:[DEPTh]

.. code-block:: python

	[SOURce<HW>]:ILS:[GSLope]:DDM:COUPling
	[SOURce<HW>]:ILS:[GSLope]:DDM:CURRent
	[SOURce<HW>]:ILS:[GSLope]:DDM:DIRection
	[SOURce<HW>]:ILS:[GSLope]:DDM:LOGarithmic
	[SOURce<HW>]:ILS:[GSLope]:DDM:PCT
	[SOURce<HW>]:ILS:[GSLope]:DDM:POLarity
	[SOURce<HW>]:ILS:[GSLope]:DDM:STEP
	[SOURce<HW>]:ILS:[GSLope]:DDM:[DEPTh]



.. autoclass:: RsSmab.Implementations.Source.Ils.Gslope.Ddm.DdmCls
	:members:
	:undoc-members:
	:noindex: