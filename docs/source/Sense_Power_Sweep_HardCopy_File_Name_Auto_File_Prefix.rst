Prefix
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:[FILE]:PREFix:STATe
	single: SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:[FILE]:PREFix

.. code-block:: python

	SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:[FILE]:PREFix:STATe
	SENSe:[POWer]:SWEep:HCOPy:FILE:[NAME]:AUTO:[FILE]:PREFix



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.HardCopy.File.Name.Auto.File.Prefix.PrefixCls
	:members:
	:undoc-members:
	:noindex: