Sonce
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:POWer:ALC:SONCe

.. code-block:: python

	[SOURce<HW>]:POWer:ALC:SONCe



.. autoclass:: RsSmab.Implementations.Source.Power.Alc.Sonce.SonceCls
	:members:
	:undoc-members:
	:noindex: