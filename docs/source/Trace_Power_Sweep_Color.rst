Color
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:COLor

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:COLor



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Color.ColorCls
	:members:
	:undoc-members:
	:noindex: