Top
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe<CH>:[POWer]:SWEep:MEASurement:POWer:PULSe:TOP

.. code-block:: python

	TRACe<CH>:[POWer]:SWEep:MEASurement:POWer:PULSe:TOP



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Power.Pulse.Top.TopCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trace.power.sweep.measurement.power.pulse.top.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trace_Power_Sweep_Measurement_Power_Pulse_Top_Display.rst