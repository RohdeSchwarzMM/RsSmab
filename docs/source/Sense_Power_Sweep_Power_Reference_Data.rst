Data
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:[POWer]:SWEep:POWer:REFerence:DATA:COPY
	single: SENSe:[POWer]:SWEep:POWer:REFerence:DATA:POINts
	single: SENSe:[POWer]:SWEep:POWer:REFerence:DATA:XVALues
	single: SENSe:[POWer]:SWEep:POWer:REFerence:DATA:YVALues

.. code-block:: python

	SENSe:[POWer]:SWEep:POWer:REFerence:DATA:COPY
	SENSe:[POWer]:SWEep:POWer:REFerence:DATA:POINts
	SENSe:[POWer]:SWEep:POWer:REFerence:DATA:XVALues
	SENSe:[POWer]:SWEep:POWer:REFerence:DATA:YVALues



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Power.Reference.Data.DataCls
	:members:
	:undoc-members:
	:noindex: