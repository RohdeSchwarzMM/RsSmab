Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:ILS:SETTing:CATalog
	single: [SOURce<HW>]:ILS:SETTing:DELete
	single: [SOURce<HW>]:ILS:SETTing:LOAD
	single: [SOURce<HW>]:ILS:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:ILS:SETTing:CATalog
	[SOURce<HW>]:ILS:SETTing:DELete
	[SOURce<HW>]:ILS:SETTing:LOAD
	[SOURce<HW>]:ILS:SETTing:STORe



.. autoclass:: RsSmab.Implementations.Source.Ils.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: