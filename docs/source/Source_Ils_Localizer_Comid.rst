Comid
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:ILS:LOCalizer:COMid:DASH
	single: [SOURce<HW>]:ILS:LOCalizer:COMid:DEPTh
	single: [SOURce<HW>]:ILS:LOCalizer:COMid:DOT
	single: [SOURce<HW>]:ILS:LOCalizer:COMid:FREQuency
	single: [SOURce<HW>]:ILS:LOCalizer:COMid:LETTer
	single: [SOURce<HW>]:ILS:LOCalizer:COMid:PERiod
	single: [SOURce<HW>]:ILS:LOCalizer:COMid:REPeat
	single: [SOURce<HW>]:ILS:LOCalizer:COMid:SYMBol
	single: [SOURce<HW>]:ILS:LOCalizer:COMid:TSCHema
	single: [SOURce<HW>]:ILS:LOCalizer:COMid:[STATe]

.. code-block:: python

	[SOURce<HW>]:ILS:LOCalizer:COMid:DASH
	[SOURce<HW>]:ILS:LOCalizer:COMid:DEPTh
	[SOURce<HW>]:ILS:LOCalizer:COMid:DOT
	[SOURce<HW>]:ILS:LOCalizer:COMid:FREQuency
	[SOURce<HW>]:ILS:LOCalizer:COMid:LETTer
	[SOURce<HW>]:ILS:LOCalizer:COMid:PERiod
	[SOURce<HW>]:ILS:LOCalizer:COMid:REPeat
	[SOURce<HW>]:ILS:LOCalizer:COMid:SYMBol
	[SOURce<HW>]:ILS:LOCalizer:COMid:TSCHema
	[SOURce<HW>]:ILS:LOCalizer:COMid:[STATe]



.. autoclass:: RsSmab.Implementations.Source.Ils.Localizer.Comid.ComidCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.ils.localizer.comid.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Ils_Localizer_Comid_Code.rst