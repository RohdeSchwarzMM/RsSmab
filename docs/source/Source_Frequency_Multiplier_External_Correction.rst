Correction
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:CORRection:CATalog
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:CORRection:CLOSs
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:CORRection:DELete
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:CORRection:MODE
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:CORRection:SELect

.. code-block:: python

	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:CORRection:CATalog
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:CORRection:CLOSs
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:CORRection:DELete
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:CORRection:MODE
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:CORRection:SELect



.. autoclass:: RsSmab.Implementations.Source.Frequency.Multiplier.External.Correction.CorrectionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.frequency.multiplier.external.correction.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Frequency_Multiplier_External_Correction_Frequency.rst
	Source_Frequency_Multiplier_External_Correction_Power.rst
	Source_Frequency_Multiplier_External_Correction_Sensor.rst