Source
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:TRIGger:SOURce

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:TRIGger:SOURce



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.Sensor.Trigger.Source.SourceCls
	:members:
	:undoc-members:
	:noindex: