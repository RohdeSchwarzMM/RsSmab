Feed
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:[POWer]:SWEep:POWer:MARKer<CH>:FEED

.. code-block:: python

	CALCulate:[POWer]:SWEep:POWer:MARKer<CH>:FEED



.. autoclass:: RsSmab.Implementations.Calculate.Power.Sweep.Power.Marker.Feed.FeedCls
	:members:
	:undoc-members:
	:noindex: