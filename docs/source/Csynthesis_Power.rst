Power
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CSYNthesis:POWer

.. code-block:: python

	CSYNthesis:POWer



.. autoclass:: RsSmab.Implementations.Csynthesis.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.csynthesis.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Csynthesis_Power_Step.rst