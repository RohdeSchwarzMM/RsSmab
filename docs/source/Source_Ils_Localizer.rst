Localizer
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:ILS:LOCalizer:MODE
	single: [SOURce<HW>]:ILS:LOCalizer:PHASe
	single: [SOURce<HW>]:ILS:LOCalizer:PRESet
	single: [SOURce<HW>]:ILS:LOCalizer:SDM
	single: [SOURce<HW>]:ILS:LOCalizer:SOURce
	single: [SOURce<HW>]:ILS:LOCalizer:STATe

.. code-block:: python

	[SOURce<HW>]:ILS:LOCalizer:MODE
	[SOURce<HW>]:ILS:LOCalizer:PHASe
	[SOURce<HW>]:ILS:LOCalizer:PRESet
	[SOURce<HW>]:ILS:LOCalizer:SDM
	[SOURce<HW>]:ILS:LOCalizer:SOURce
	[SOURce<HW>]:ILS:LOCalizer:STATe



.. autoclass:: RsSmab.Implementations.Source.Ils.Localizer.LocalizerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.ils.localizer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Ils_Localizer_Comid.rst
	Source_Ils_Localizer_Ddm.rst
	Source_Ils_Localizer_Frequency.rst
	Source_Ils_Localizer_Icao.rst
	Source_Ils_Localizer_Llobe.rst
	Source_Ils_Localizer_Rlobe.rst