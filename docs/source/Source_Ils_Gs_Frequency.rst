Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:ILS:[GS]:FREQuency:MODE
	single: [SOURce<HW>]:ILS:[GS]:FREQuency:STEP
	single: [SOURce<HW>]:ILS:[GS]:FREQuency

.. code-block:: python

	[SOURce<HW>]:ILS:[GS]:FREQuency:MODE
	[SOURce<HW>]:ILS:[GS]:FREQuency:STEP
	[SOURce<HW>]:ILS:[GS]:FREQuency



.. autoclass:: RsSmab.Implementations.Source.Ils.Gs.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: