Comid
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:ADF:COMid:CODE
	single: [SOURce<HW>]:ADF:COMid:DASH
	single: [SOURce<HW>]:ADF:COMid:DEPTh
	single: [SOURce<HW>]:ADF:COMid:DOT
	single: [SOURce<HW>]:ADF:COMid:FREQuency
	single: [SOURce<HW>]:ADF:COMid:LETTer
	single: [SOURce<HW>]:ADF:COMid:PERiod
	single: [SOURce<HW>]:ADF:COMid:SYMBol
	single: [SOURce<HW>]:ADF:COMid:TSCHema
	single: [SOURce<HW>]:ADF:COMid:[STATe]

.. code-block:: python

	[SOURce<HW>]:ADF:COMid:CODE
	[SOURce<HW>]:ADF:COMid:DASH
	[SOURce<HW>]:ADF:COMid:DEPTh
	[SOURce<HW>]:ADF:COMid:DOT
	[SOURce<HW>]:ADF:COMid:FREQuency
	[SOURce<HW>]:ADF:COMid:LETTer
	[SOURce<HW>]:ADF:COMid:PERiod
	[SOURce<HW>]:ADF:COMid:SYMBol
	[SOURce<HW>]:ADF:COMid:TSCHema
	[SOURce<HW>]:ADF:COMid:[STATe]



.. autoclass:: RsSmab.Implementations.Source.Adf.Comid.ComidCls
	:members:
	:undoc-members:
	:noindex: