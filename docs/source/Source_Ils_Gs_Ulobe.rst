Ulobe
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:ILS:[GS]:ULOBe:[FREQuency]

.. code-block:: python

	[SOURce<HW>]:ILS:[GS]:ULOBe:[FREQuency]



.. autoclass:: RsSmab.Implementations.Source.Ils.Gs.Ulobe.UlobeCls
	:members:
	:undoc-members:
	:noindex: