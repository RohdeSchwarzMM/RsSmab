Remove
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALibration:DATA:REMove

.. code-block:: python

	CALibration:DATA:REMove



.. autoclass:: RsSmab.Implementations.Calibration.Data.Remove.RemoveCls
	:members:
	:undoc-members:
	:noindex: