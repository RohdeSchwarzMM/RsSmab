Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:VOR:SETTing:CATalog
	single: [SOURce<HW>]:VOR:SETTing:DELete
	single: [SOURce<HW>]:VOR:SETTing:LOAD
	single: [SOURce<HW>]:VOR:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:VOR:SETTing:CATalog
	[SOURce<HW>]:VOR:SETTing:DELete
	[SOURce<HW>]:VOR:SETTing:LOAD
	[SOURce<HW>]:VOR:SETTing:STORe



.. autoclass:: RsSmab.Implementations.Source.Vor.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: