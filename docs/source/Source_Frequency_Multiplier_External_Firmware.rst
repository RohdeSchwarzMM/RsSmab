Firmware
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:FIRMware:CATalog
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:FIRMware:SELect
	single: [SOURce<HW>]:FREQuency:MULTiplier:EXTernal:FIRMware:VERSion

.. code-block:: python

	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:FIRMware:CATalog
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:FIRMware:SELect
	[SOURce<HW>]:FREQuency:MULTiplier:EXTernal:FIRMware:VERSion



.. autoclass:: RsSmab.Implementations.Source.Frequency.Multiplier.External.Firmware.FirmwareCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.frequency.multiplier.external.firmware.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Frequency_Multiplier_External_Firmware_Update.rst