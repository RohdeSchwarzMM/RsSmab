Srange
----------------------------------------





.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Frequency.Sensor.Srange.SrangeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.sweep.frequency.sensor.srange.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Power_Sweep_Frequency_Sensor_Srange_Start.rst
	Sense_Power_Sweep_Frequency_Sensor_Srange_State.rst
	Sense_Power_Sweep_Frequency_Sensor_Srange_Stop.rst