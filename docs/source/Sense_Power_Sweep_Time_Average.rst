Average
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:[POWer]:SWEep:TIME:AVERage:[COUNt]

.. code-block:: python

	SENSe:[POWer]:SWEep:TIME:AVERage:[COUNt]



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: