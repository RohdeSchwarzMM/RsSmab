State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:PULSe:STATe

.. code-block:: python

	SENSe<CH>:[POWer]:SWEep:TIME:[SENSor]:PULSe:STATe



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Time.Sensor.Pulse.State.StateCls
	:members:
	:undoc-members:
	:noindex: