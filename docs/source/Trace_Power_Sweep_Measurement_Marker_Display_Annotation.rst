Annotation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe:[POWer]:SWEep:MEASurement:MARKer:DISPlay:ANNotation:[STATe]

.. code-block:: python

	TRACe:[POWer]:SWEep:MEASurement:MARKer:DISPlay:ANNotation:[STATe]



.. autoclass:: RsSmab.Implementations.Trace.Power.Sweep.Measurement.Marker.Display.Annotation.AnnotationCls
	:members:
	:undoc-members:
	:noindex: