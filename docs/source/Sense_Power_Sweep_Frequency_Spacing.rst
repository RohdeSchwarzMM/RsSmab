Spacing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:[POWer]:SWEep:FREQuency:SPACing:[MODE]

.. code-block:: python

	SENSe:[POWer]:SWEep:FREQuency:SPACing:[MODE]



.. autoclass:: RsSmab.Implementations.Sense.Power.Sweep.Frequency.Spacing.SpacingCls
	:members:
	:undoc-members:
	:noindex: