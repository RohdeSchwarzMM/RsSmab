Offset
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CSYNthesis:OFFSet:STATe
	single: CSYNthesis:OFFSet

.. code-block:: python

	CSYNthesis:OFFSet:STATe
	CSYNthesis:OFFSet



.. autoclass:: RsSmab.Implementations.Csynthesis.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: